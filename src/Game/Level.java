/**
    The Level class contains the data for a tile-based
    map, including Sprites. Each tile is a reference to an
    Image. Of course, Images are used multiple times in the tile
    map.
*/

package Game;

import Game.Sprites.*;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.Iterator;
import physics.RigidPhysics.*;

public class Level {

//    private Image[][] tiles;
//    private Image[][] details;
    private LinkedList<Sprite> sprites;
    private Player player;
    private BufferedImage background;
	private BufferedImage foreground;
	private int width, height;
	public LevelMetadata metadata;
    
	/* ------------------------------------------
	 * Here is stuff needed for physics
	 * ------------------------------------------
	 */
	protected World			physicsWorld	= new World();
	
	/* -------------------------------------------------------------------------
	 * Constructor: Creates a new Level with the specified width and height
	 * (in number of tiles) of the map.
	 * -------------------------------------------------------------------------
	 */
    public Level(int w, int h) {
		width = w; height = h;
        sprites = new LinkedList<Sprite>();
        metadata = new LevelMetadata();
	}
	
    /* -------------------------------------------------------------------------
	 * A deep coping constructor of level
	 * -------------------------------------------------------------------------
	 */
	public Level(Level level) {
		width = level.getWidth(); 
        height = level.getHeight();
		sprites = new LinkedList<Sprite>();
		System.out.println("A");
        int count=1;
		for(Sprite current: level.sprites) {
            if (count >5 )break;
            try{
                
            System.out.println("B");
            System.out.println("sprite: "+current);
			sprites.add(new Sprite(current));
            }catch(Exception e){
                e.printStackTrace();
                break;
            }
		}
        System.out.println("C");
        background = level.background;
        foreground = level.foreground;
//        player = new Player(level.player);
        metadata = level.metadata; //can use same reference since it is just "static" data
	}
	
	
	/* -------------------------------------------------------------------------
	 * Setters and getters
	 * -------------------------------------------------------------------------
	 */
	public int getWidth()	{return width;}
	public int getHeight()	{return height;}
	
	// TODO: remove these, deprecated
//	public	void	setTile(int x, int y, Image tile)	{tiles[x][y] = tile;}
//	public	void	setDetail(int x, int y, Image tile)	{details[x][y] = tile;}

	public	Sprite	getPlayer()				{return player;}
	public	void	setPlayer(Player player){this.player = player;physicsWorld.addBody(player.getBody());}

	// Note that player is not part of sprites
	public	void	addSprite(Sprite sprite)	{sprites.add(sprite);	physicsWorld.addBody(sprite.getBody());}
	public	void	removeSprite(Sprite sprite) {sprites.remove(sprite);physicsWorld.remove(sprite.getBody());}
	public	Iterator getSprites()				{return sprites.iterator();}

	void	setBackground(BufferedImage b) {background = b;}
	public	BufferedImage getBackground(){return background;}
	
	void	setForeground(BufferedImage b)	{foreground = b;}
	public	BufferedImage getForeground()	{return foreground;}
	
	/* -------------------------------------------------------------------------
	 * Gets the Sprite with the given RigidBody
	 * -------------------------------------------------------------------------
	 */
	public Sprite getSprite(RigidBody body) {
		if(body == null)				{return null;}
		if(player.getBody() == body)	{return player;}
		
		int length = sprites.size();
		Sprite current;
		for(int i=0;i<length;i++) {
			current = (Sprite)sprites.get(i);
			if(current.getBody() == body){return current;}
		}
		
		return null;
	}
}
