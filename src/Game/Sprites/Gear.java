
package Game.Sprites;

import java.awt.Image;

public class Gear extends DynamicGameProp{

	public Gear(Image im, int i, int j) {
		super(im,i,j);
		body.setBoundingCircle(16.0f);
		body.setContactCircle(16.0f);
	}
}
