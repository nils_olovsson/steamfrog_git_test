package Game.Sprites;

import java.lang.reflect.Constructor;

/**
    A Creature is a Sprite that is affected by gravity and can
    die. It has four Animations: moving left, moving right,
    dying on the left, and dying on the right.
*/
public abstract class Creature extends Sprite {
	
	// Amount of time to go from STATE_DYING to STATE_DEAD.
    protected static final int DIE_TIME = 1000;

    public static final int STATE_NORMAL = 0;
    public static final int STATE_DYING = 1;
    public static final int STATE_DEAD = 2;
    public static final int STATE_RUNNING = 3;
    public static final int STATE_JUMPING = 4;

    protected Animation left, right, deadLeft, deadRight;
    protected Animation idleLeft, idleRight, jumpLeft, jumpRight;
    protected int state;
    protected long stateTime;
	
	/* -------------------------------------------------------------------------
	 * Constructors:
	 * 1. Creates a new Creature with the specified Animations.
	 * 2. Creates a new Creature with the specified Animations. Used for player?
	 * -------------------------------------------------------------------------
	 */
    public Creature(Animation left, Animation right,
					Animation deadLeft, Animation deadRight) {
		super(right);
		this.left = left;			this.right = right;
		this.deadLeft = deadLeft;	this.deadRight = deadRight;
		state = STATE_NORMAL;
    }
	
	/* -------------------------------------------------------------------------
	 * Clone this object
	 * -------------------------------------------------------------------------
	 */
    public Object clone() {
        // use reflection to create the correct subclass
        Constructor constructor = getClass().getConstructors()[0];
        try {
            return constructor.newInstance(new Object[] {
                (Animation)left.clone(),
                (Animation)right.clone(),
                (Animation)deadLeft.clone(),
                (Animation)deadRight.clone()
            });
        }
        catch (Exception ex) {
            // should never happen
            ex.printStackTrace();
            return null;
        }
    }
	
	/* -------------------------------------------------------------------------
	 * Getters
	 * -------------------------------------------------------------------------
	 */
    public float getMaxSpeed()	{return 0;}
    public int getState()		{return state;}
	
	/* -------------------------------------------------------------------------
	 * Wakes up the creature when the Creature first appears on screen.
	 * Normally, the creature starts moving left.
	 * -------------------------------------------------------------------------
	 */
    public void wakeUp() {
        if (getState() == STATE_NORMAL && getVelocityX() == 0) {
            setVelocityX(-getMaxSpeed());
        }
    }
	
	/* -------------------------------------------------------------------------
	 * Sets state of this Creature to STATE_NORMAL, STATE_DYING, or STATE_DEAD.
	 * -------------------------------------------------------------------------
	 */
    public void setState(int state) {
        if (this.state != state) {
			this.state = state; stateTime = 0;
			if (state == STATE_DYING) {setVelocityX(0);setVelocityY(0);}
		}
	}
	
	public void setDead() {state = STATE_DEAD;}
	
	/* -------------------------------------------------------------------------
	 * Various checks for different creature states
	 * -------------------------------------------------------------------------
	 */
	public boolean isAlive()	{return (state != STATE_DEAD);}
	public boolean isFlying()	{return false;}
	public boolean isRunning()	{return (state == STATE_RUNNING);}
	public boolean isJumping()	{return (state == STATE_JUMPING);}
	
	/* -------------------------------------------------------------------------
	 * Called before update() if the creature collided with a tile horizontally
	 * or verically.
	 * -------------------------------------------------------------------------
	 */
    public void collideHorizontal()	{setVelocityX(-getVelocityX());}
    public void collideVertical()	{setVelocityY(0);}
	
	/* -------------------------------------------------------------------------
	 * Updates the animaton for this creature.
	 * -------------------------------------------------------------------------
	 */
    public void update(long elapsedTime) {
        // select the correct Animation
        Animation newAnim = anim;
        if		(getVelocityX() < 0) {newAnim = left;}
		else if	(getVelocityX() > 0) {newAnim = right;}
        
		if		(state==STATE_DYING && newAnim==left)	{newAnim = deadLeft;}
		else if (state==STATE_DYING && newAnim==right)	{newAnim = deadRight;}

        // update the Animation
        if (anim != newAnim)	{anim = newAnim; anim.start();
        }else					{anim.update(elapsedTime);}

        // update to "dead" state
        stateTime += elapsedTime;
        if (state==STATE_DYING && stateTime>=DIE_TIME) {setState(STATE_DEAD);}
    }

}
