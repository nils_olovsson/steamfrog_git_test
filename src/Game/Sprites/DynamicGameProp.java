/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Game.Sprites;

import Game.TileMapRenderer;
import java.awt.Image;
import physics.RigidPhysics.NarrowCollision;

public class DynamicGameProp extends Sprite{
	
	private boolean isGrabbed;
	
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
	public DynamicGameProp(Image im, int i, int j) {
		super();
		if(anim==null){anim = new Animation();}
		anim.addFrame(im, 1);
		
		body.setInverseMass(0.5f);
		body.setInverseInertia(0.0005f);
		body.setFriction(1.0f);
		body.setRestitution(0.2f);
        
		float x = TileMapRenderer.tilesToPixels(i)+16;
		float y = TileMapRenderer.tilesToPixels(j)+16;
		
        body.setPosition(x, y);
		
		isGrabbed = false;
	}
	
	/* -------------------------------------------------------------------------
	 * Small methods thay may be doing nothing for these subclasses
	 * -------------------------------------------------------------------------
	 */
	public void	 update	(long elapsedTime)	{}
	public int	 getWidth()					{return 32;}
	public int	 getHeight()				{return 32;}
	public boolean isGrabbed()	{return isGrabbed;}
	public void grab()			{isGrabbed = !isGrabbed;}
	public boolean collide(Sprite otherSprite, NarrowCollision.Contact contact){
		if(isGrabbed)	{return true;}
		else			{return false;}
	}
}
