/**
 * Platform
 * This is a physical construct with no active rendering function.
 * This means that other sprites can collide with it and interact with it. It
 * does not have any actual rendering properties.
 * 
 * All platforms are static. This means that they can't be moved.
 */

package Game.Sprites;

import Game.TileMapRenderer;
import java.awt.Graphics2D;
import java.awt.Image;

public class Platform extends Sprite{
	
	private int type;
	
	/* -------------------------------------------------------------------------
	 * The main Constructor
     * takes world coordinates
	 * -------------------------------------------------------------------------
	 */
	public Platform(int i, int j, int type) {
		super();
        
		this.type = type;
		body.setStatic();
		body.setFriction(0.0f);
		body.setRestitution(0.0f);
        
		float x = TileMapRenderer.tilesToPixels(i)+16;
		float y = TileMapRenderer.tilesToPixels(j)+16;
		
        body.setPosition(x, y);

		// ----------------------------------------------------------
		// Creates the correct contact geometry for this platform.
		// If tilting platforms behave weird, maybe make them a
		// liiiiiitle thicker, really small things may fall through
		// them though. And things can still travel in under them.
		// ----------------------------------------------------------
		if(type<0){type*=-1;} type%=3;
		switch(type) {
			case(0):
				body.buildContactGeometry(16.0f, 16.0f);
				break;
			case(1):
				body.buildContactGeometry(22.5f, 0.5f);
				body.setOrientation(1.0f, -1.0f);
				body.setPositionY(body.getPosition().y+1.0f);
				break;
			case(2):
				body.buildContactGeometry(22.5f, 0.5f);
				body.setOrientation(1.0f,1.0f);
				body.setPositionY(body.getPosition().y+1.0f);
				break;
		}
	}
    /* -------------------------------------------------------------------------
	 * Constructor, takes index coordinates
	 * -------------------------------------------------------------------------
	 */
    public Platform(int x, int y, String type){
        this(x,y,
                (type.equalsIgnoreCase("slopeLeft"))?1:
                (type.equalsIgnoreCase("slopeRight"))?2:0
                );
    }
	
	/* -------------------------------------------------------------------------
	 * Small methods
	 * -------------------------------------------------------------------------
	 */
	public void		render	(Graphics2D g2d)	{}
	public void		update	(long elapsedTime)	{}
	public int		getWidth()					{return 32;}
	public int		getHeight()					{return 32;}
	public Image	getImage()					{return null;}
	public int		getType()					{return type;}
}
