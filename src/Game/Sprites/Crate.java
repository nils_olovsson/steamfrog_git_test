
package Game.Sprites;

import java.awt.Image;

public class Crate extends DynamicGameProp{
	
	public Crate(Image im, int i, int j) {
		super(im, i, j);
		body.buildContactGeometry(16.0f, 16.0f);
	}
}
