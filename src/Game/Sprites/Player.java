/**
 * Player
 */

package Game.Sprites;

import java.util.ArrayList;
import physics.RigidPhysics.NarrowCollision.Contact;
import physics.RigidPhysics.RigidBody;
import soundengine.Sound;

public class Player extends Creature {
	
	Sound jumpSfx;
	
    private static final float JUMP_SPEED = -175.0f;
	private static final float WALK_SPEED =  100.0f;
    private boolean onGround, freeLeft, freeRight;
	public  boolean isAtGoal = false;
	private long onGroundCounter,freeLeftCounter, freeRightCounter; 
	
	ArrayList potentialGrabbers;
	Sprite grabbed;
	private float grabbedInverseMass;
	private float grabbedInverseInertia;
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
    public Player(	Animation left, Animation right,
					Animation deadLeft, Animation deadRight) {
        // TODO: Temporary fix here to make animations correct, fix at source of error instead!
		super(right, left, deadLeft, deadRight);
		
		this.setY(this.getY()+32.0f);
		body.setInverseMass(1.0f);
		body.setInverseInertia(0.0f);
		body.setRestitution(0.0f);
		body.buildContactGeometry(12.0f, 14.0f); // Build block size of tile
		
		potentialGrabbers = new ArrayList<Sprite>();
		
		loadJumpsfx();
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
	public String toString() {
		String str = "";
		if(onGround) {str = str + "Player is on ground\n";}
		else		 {str = str + "Player is NOT on ground\n";}
		
		if(freeLeft) {str = str + "Player is free on left\n";}
		else		 {str = str + "Player is NOT free on left\n";}
		
		if(freeLeft) {str = str + "Player is free on right\n";}
		else		 {str = str + "Player is NOT free on right\n";}
		
		return str;
	}
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
	public void update(long elapsedTime) {
        // select the correct Animation
        Animation newAnim = anim;
		if			(getVelocityX() < 0) {newAnim = left;
		} else if	(getVelocityX() > 0) {newAnim = right;}
		
		if		(state==STATE_DYING && newAnim==left)	{newAnim = deadLeft;
        }else if(state==STATE_DYING && newAnim==right)	{newAnim = deadRight;}
		
        // update the Animation
        if (anim != newAnim)	{anim = newAnim; anim.start();
        }else					{anim.update(elapsedTime);}
		
        // update to "dead" state
		stateTime += elapsedTime;
		if (state==STATE_DYING && stateTime>=DIE_TIME) {setState(STATE_DEAD);}
		
		// These are reset to false almost every frame
		onGroundCounter += elapsedTime;
		freeLeftCounter += elapsedTime;
		freeRightCounter+= elapsedTime;
		if(onGroundCounter < 200) {onGround = true;}
		else {onGround = false;}
		
		if(freeLeftCounter<200) {freeLeft = false;}
		else {freeLeft = true;}
		
		if(freeRightCounter<200) {freeRight = false;}
		else {freeRight = true;}
		
		isAtGoal = false;
		
		// ----------------------------------------------------------
		// Now do changes on picked upp item
		// ----------------------------------------------------------
		potentialGrabbers.clear();
		if(grabbed != null) {
			grabbed.setY(this.getY());
			if(anim == left)	{grabbed.setX(this.getX()-15.0f);}
			if(anim == right)	{grabbed.setX(this.getX()+15.0f);}
		}
    }

    public void wakeUp() {}
	public void setOnGround() {onGround = true; onGroundCounter = 0;}
	public void setOffGround() {onGround = false; onGroundCounter = 201;}
	private void loadJumpsfx() {jumpSfx = new Sound("materials/sounds/jump.wav",-10.0f);}
	
	/* -------------------------------------------------------------------------
	 * Makes player jump if the player is on the ground or if forceJump is true.
	 * -------------------------------------------------------------------------
	 */
    public void jump(boolean jump) {
        if (onGround && jump) {
			this.setOffGround();
			setVelocityY(JUMP_SPEED);
			if(jumpSfx!=null){jumpSfx.play();}
			loadJumpsfx();
		}
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
	public void walkL(boolean walk) {
        if (walk) {this.setVelocityX(-WALK_SPEED);}
    }
	
	public void walkR(boolean walk) {
        if (walk) {this.setVelocityX(WALK_SPEED);}
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public float getMaxSpeed() {return WALK_SPEED;}
	
	public void grab() {
			if(grabbed==null && potentialGrabbers.size()>0) {
				grabbed = (Sprite)potentialGrabbers.get(0);
				grabbedInverseMass		= grabbed.getBody().getInverseMass();
				grabbedInverseInertia	= grabbed.getBody().getInverseInertia();
				grabbed.getBody().setOrientation(1.0f,0.0f);
				grabbed.getBody().setRotation(0.0f);
				grabbed.getBody().setVelocity(0.0f,0.0f);
				grabbed.getBody().setStatic();
				DynamicGameProp dynamo = (DynamicGameProp)grabbed;
				dynamo.grab();
			}else if(grabbed != null) {
				grabbed.getBody().setInverseMass(grabbedInverseMass);
				grabbed.getBody().setInverseInertia(grabbedInverseInertia);
				
				DynamicGameProp dynamo = (DynamicGameProp)grabbed;
				dynamo.grab();
				grabbed = null;
			}
	}
	
	/* -------------------------------------------------------------------------
	 * Here is collision game logic applied for Player
	 * First check what we are colliding with
	 * -------------------------------------------------------------------------
	 */
	public boolean collide(Sprite otherSprite, Contact contact) {
		
		if(otherSprite instanceof PowerUp) {
			return collidePowerUp(otherSprite);
		} else if (otherSprite instanceof Platform) {
			return collidePlatform(otherSprite,contact);
		} else if  (otherSprite instanceof SpringElevator || otherSprite instanceof DynamicGameProp) {
			return collidePlatform(otherSprite,contact);
		} else if(otherSprite instanceof StaticGameProp) {
			return true;
		}
		
		return false;
	}
	
	/* -------------------------------------------------------------------------
	 * This collides the player against a platform.
	 * Can set if player is in contact with ground if normal of contact is
	 * mostly in y-direction.
	 * -------------------------------------------------------------------------
	 */
	public boolean collidePlatform(Sprite otherSprite, Contact contact) {
		
		if(otherSprite instanceof DynamicGameProp) {
			if(grabbed == otherSprite && grabbed!=null){return true;}
			if(grabbed == null)		{potentialGrabbers.add(otherSprite);}
		}
		
		if(Math.abs(contact.contactNormal.y) > 0.1){this.setOnGround();}
		else if(!(otherSprite instanceof DynamicGameProp)) {
			if(otherSprite.getX() < this.getX()) {
				freeLeft  = false; freeLeftCounter=0;
			}else if(otherSprite.getX() > this.getX()) {
				freeRight = false; freeRightCounter=0;
			}
		}
		return false;
	}
	
	/* -------------------------------------------------------------------------
	 * Pick up powerup and remove player from contact, powerup removes itself
	 * -------------------------------------------------------------------------
	 */
	public boolean collidePowerUp(Sprite otherSprite) {
		if(otherSprite instanceof PowerUp.Goal) {isAtGoal = true;}
		
		return true;
	}
}