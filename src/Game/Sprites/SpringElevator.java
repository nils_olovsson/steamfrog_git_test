/**
 * Sprite class for a kind of elevator in game. In order to initialize it
 * correctly its bungee must be added to the force generator registry in
 * the physics World together with the elevator instance that contains it.
 * 
 * TODO: Test to see what values should be used for various things.
 */


package Game.Sprites;

import Game.TileMapRenderer;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import physics.Math.Vector2;
import physics.RigidPhysics.ForceGenerator.*;
import physics.RigidPhysics.NarrowCollision;

public class SpringElevator extends Sprite{
	
	private BungeeRope bungee;
	
	private boolean onGround;
	private long onGroundCounter;
	
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
	public SpringElevator(Image im, int i, int j) {
		super();
		if(anim==null){anim = new Animation();}
		anim.addFrame(im, 1);
		
		body.setInverseMass(1.0f);
		body.setInverseInertia(0.0f);
		body.setFriction(0.1f);
		body.setRestitution(0.0f);
		body.setLinearDamping(0.9f);
        
		float x = TileMapRenderer.tilesToPixels(i)+16;
		float y = TileMapRenderer.tilesToPixels(j)+16;
		
        body.setPosition(x, y);
		body.buildContactGeometry(47.0f,8.0f);
		
		bungee = new BungeeRope();
		bungee.setRestLength(16.0f);
		bungee.setSpringConstant(17.0f);
		bungee.setConnectionPoint2(x, y-48.f);
	}
	
	/* -------------------------------------------------------------------------
	 * Small methods thay may be doing nothing for these subclasses
	 * -------------------------------------------------------------------------
	 */
	public BungeeRope getBungeeRope() {return bungee;}
	public int	 getWidth()					{return 96;}
	public int	 getHeight()				{return 16;}
	public void setOnGround() {onGround = true; onGroundCounter = 0;}
	public void setOffGround() {onGround = false;}
	
	public void update(long elapsedTime) {
		onGroundCounter += elapsedTime;
		if(onGroundCounter < 40)	{onGround = true;}
		else						{onGround = false;}
	}
	
	/* -------------------------------------------------------------------------
	 * Rendering the object as an ordinary sprite but also renders two ropes
	 * that is connected to this elevator.
	 * NOTE: Two ropes are rendered at corners of elevator but only one is
	 * physically connected and that is connected to center of elevator.
	 * -------------------------------------------------------------------------
	 */	
	public void render(Graphics2D g2d) {
		if(this.getImage() == null) {return;}
		AffineTransform stack = g2d.getTransform();
		AffineTransform trans = g2d.getTransform();
		
		// Center off sprite, rotation around this point
		int x = Math.round(this.getX());
		int y = Math.round(this.getY());
		
		// For drawing rectangles at correct position
		int drawX	= (int)Math.round(this.getX() - this.getWidth() *0.5);
		int drawY	= (int)Math.round(this.getY() - this.getHeight()*0.5);
		
		// Apply rotation tranform, then draw, then reset transform
		trans.rotate(body.getOrientationRad(), x, y);
		g2d.setTransform(trans);
		g2d.drawImage(this.getImage(), drawX, drawY, null);
		g2d.setTransform(stack);
		
		// Now draw two lines that represent ropes.
		float hWidth	= 47.0f;
		float hHeight	= 7.0f;
		Vector2 connection1 = new Vector2(hWidth, -hHeight);	// Upper right
		Vector2 connection2 = new Vector2(-hWidth,-hHeight);	// Upper left
		body.getTransform().transUpdate(connection1);		// Get connections
		body.getTransform().transUpdate(connection2);		// in world coord
		
		g2d.setColor(Color.getHSBColor(19.0f, 0.7f, 0.4f));
		x = (int)(bungee.getConnectionPoint2().x+hWidth);
		y = (int)(bungee.getConnectionPoint2().y);
		drawX = (int)(connection1.x);
		drawY = (int)(connection1.y);
		g2d.drawLine(drawX, drawY, x, y);
		
		x = (int)(bungee.getConnectionPoint2().x-hWidth);
		drawX = (int)(connection2.x);
		drawY = (int)(connection2.y);
		g2d.drawLine(drawX, drawY, x, y);
	}
	
	public boolean collide(Sprite otherSprite, NarrowCollision.Contact contact) {
		
		if (otherSprite instanceof Platform) {
			if(Math.abs(contact.contactNormal.y) > 0.1){this.setOnGround();}
		} /*else if(otherSprite instanceof DynamicGameProp && this.onGround) {
			return true;
		}*/
		return false;
	}
}
