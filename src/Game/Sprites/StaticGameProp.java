/**
 * Game property that can be rendered but not directly interacted with since it
 * lacks any contact geometry and is static.
 * Have two subclasses BGTile and BGDetail, BGDetail is supposed to be rendered
 * infront of BGTile.
 */
package Game.Sprites;

import Game.TileMapRenderer;
import java.awt.Image;
import physics.RigidPhysics.NarrowCollision.Contact;

public class StaticGameProp extends Sprite{
	
	/* -------------------------------------------------------------------------
	 * Two inner classes that behave in same way but are separate because of
	 * level layering conventions and rendering. Their only difference except
	 * from what images they may have is their depth coordinate:
	 * 1. BackgroundTile
	 * 2. StaticDetail
	 * -------------------------------------------------------------------------
	 */
	public static class BGTile extends StaticGameProp {
		public BGTile(int i, int j, Image im) {super(im, i, j); depthCoord=2;}
	}
	
	public static class BGDetail extends StaticGameProp {
		public BGDetail(int i, int j, Image im) {super(im, i, j); depthCoord=3;}
	}
	
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
	public StaticGameProp(Image im, int i, int j) {
		super();
		if(anim == null) {anim = new Animation();}
		anim.addFrame(im, 1); // Second parameter never used for this animation
		body.setStatic();
		body.setNoContactGeometry();
		
		// Calculate position of platform from tile coordinates
		int x = TileMapRenderer.tilesToPixels(i)+16;
		int y = TileMapRenderer.tilesToPixels(j)+16;
		
		body.setPosition(x,y);
	}
	
	/* -------------------------------------------------------------------------
	 * Small methods thay may be doing nothing for these subclasses
	 * -------------------------------------------------------------------------
	 */
	public void	 update	(long elapsedTime)	{}
	public int	 getWidth()					{return 32;}
	public int	 getHeight()				{return 32;}
	public boolean collide(Sprite otherSprite, Contact contact) {return true;}
}