/**
    A PowerUp class is a Sprite that the player can pick up.
*/

package Game.Sprites;

import java.lang.reflect.Constructor;
import physics.RigidPhysics.NarrowCollision;

public abstract class PowerUp extends Sprite {
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
    public PowerUp(Animation anim) {super(anim);}
	
	/* -------------------------------------------------------------------------
	 * Clone
	 * -------------------------------------------------------------------------
	 */
    public Object clone() {
        // use reflection to create the correct subclass
        Constructor constructor = getClass().getConstructors()[0];
        try {
            return constructor.newInstance(
					new Object[] {(Animation)anim.clone()});
        }
        catch (Exception ex) {ex.printStackTrace();return null;}
    }
	
	public boolean collide(Sprite otherSprite, NarrowCollision.Contact contact) {
		if(otherSprite instanceof Platform){return false;}
		else {return true;}
	}
	
	/* -------------------------------------------------------------------------
	 * Inner classes for some powerups
	 * -------------------------------------------------------------------------
	 */
	
    public static class Goal extends PowerUp {
        public Goal(Animation anim) {
			super(anim);
			this.body.setInverseInertia(0.0f);
			this.body.setFriction(0.0f);
		}
    }

}