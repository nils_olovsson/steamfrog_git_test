/**
    The ResourceManager class loads and manages tile Images and
    "host" Sprites used in the game. Game Sprites are cloned from
    "host" Sprites.
*/

package Game;

import Game.Sprites.Animation;
import Game.Sprites.Player;
import Game.Sprites.PowerUp;
import Game.Sprites.Sprite;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.imageio.ImageIO;

public class ResourceManager {
	
    private ArrayList tiles;
    private int currentMap;
    private GraphicsConfiguration gc;

    // host sprites used for cloning
    private static Player playerSprite;
    private static Sprite goalSprite;

    private LevelManager levelManager;
    private Level currentLevel;
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public static Sprite	getPlayerSprite()	{return playerSprite;}
    public Level		getCurrentLevel()	{return currentLevel;}
	
	/* -------------------------------------------------------------------------
	 * Constructor: Creates ResourceManager with specified GraphicsConfiguration
	 * -------------------------------------------------------------------------
	 */
    public ResourceManager(GraphicsConfiguration gc) {
        this.gc = gc;
        levelManager = new LevelManager();
        loadTileImages();
        loadCreatureSprites();
        loadPowerUpSprites();
        levelManager.initiateAllLevels();
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public static BufferedImage loadImage(String path) {
        BufferedImage img = null;
        try {
            File f  = new File(path);
            if(!f.exists()){
                System.out.println("image not found at path "+path);
                return null;
            }
            img = ImageIO.read(f);
            if(img==null){
                System.out.println("Problem finding image file "+f.getAbsolutePath());
            }
            return img;
            
        } catch (IOException ex) {
            System.out.println("image not found at path "+path);
        }
        return img;
    }
    
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public Image[] loadSpriteSheet(	String resourcePath,
									int rows, int cols,
									int width, int height) {
        
		BufferedImage[] images = new BufferedImage[rows * cols];
		BufferedImage img = null;
		
        File f  = new File(resourcePath);
        img = loadImage(resourcePath);
		

		if (rows == 1) {
			for (int i = 0; i < cols; i++) {
				images[i] = img.getSubimage(i*width, 0, width, height);
			}
		}else {
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					images[(i * cols) + j] = 
							img.getSubimage(i*width, j * height, width, height);
				}
			}
		}
        
		return images;
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public static BufferedImage createBackgroundImage(	String path, 
														int width, int height) {
		BufferedImage orginal = loadImage(path);
		double factor = height/orginal.getHeight();

		int newImageWidth = (int)(orginal.getWidth()*factor+0.5)+1;

		BufferedImage baseImage = loadImageOfSize(path, newImageWidth, height);

		int type = BufferedImage.TYPE_INT_ARGB;
		BufferedImage bkgImage = new BufferedImage(width, height, type);
		Graphics2D g = bkgImage.createGraphics();

		//draw as many images as fits
		for(int n=0; n<=width; n+=newImageWidth){
			g.drawImage(baseImage, n, 0, newImageWidth, height, null);
		}
		
		g.dispose();	
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
		RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		RenderingHints.VALUE_ANTIALIAS_ON);

		return bkgImage;
    }
    
	/* -------------------------------------------------------------------------
	 * Returns a BufferedImage with the size of a tile,
	 * that is with and height = TileMapRenderer.TILE_SIZE 
	 * -------------------------------------------------------------------------
	 */
    public static BufferedImage loadTileSizedImage(String path) {
        int TILE_SIZE = TileMapRenderer.TILE_SIZE;
        return loadImageOfSize(path, TILE_SIZE, TILE_SIZE);
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public static BufferedImage loadImageOfSize(String path,
												int width, int height) {
        BufferedImage img = null;
        
            
            img = loadImage(path);
            
            //int type = img.getType() == 0? BufferedImage.TYPE_INT_ARGB : img.getType();
            int type = BufferedImage.TYPE_INT_ARGB;
            BufferedImage resizedImage = new BufferedImage(width, height, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(img, 0, 0, width, height, null);
            g.dispose();	
            g.setComposite(AlphaComposite.Src);

            g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
            RenderingHints.VALUE_INTERPOLATION_BILINEAR);
            g.setRenderingHint(RenderingHints.KEY_RENDERING,
            RenderingHints.VALUE_RENDER_QUALITY);
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
 
            return resizedImage;
            
        
     
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
	public Image getMirrorImage(Image image) {
		return getScaledImage(image,-1,1);
	}
	
	public Image getFlippedImage(Image image) {
		return getScaledImage(image,1,-1);
	}

	private void addGoalSprite(Level level) {
		int[] goal = currentLevel.metadata.goal;
		int x = goal[0], y = goal[1];
		addSprite(currentLevel, goalSprite, x, y);
	}
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    private Image getScaledImage(Image image, float x, float y) {

        // set up the transform
        AffineTransform transform = new AffineTransform();
        transform.scale(x, y);
        transform.translate(
            (x-1) * image.getWidth(null) / 2,
            (y-1) * image.getHeight(null) / 2);

        // create a transparent (not translucent) image
        Image newImage = gc.createCompatibleImage(
            image.getWidth(null),
            image.getHeight(null),
            Transparency.BITMASK);

        // draw the transformed image
        Graphics2D g = (Graphics2D)newImage.getGraphics();
        g.drawImage(image, transform, null);
        g.dispose();

        return newImage;
    }
    
    
    /* -------------------------------------------------------------------------
	 * Helper method for the loadNextMap and reloadMap methods. 
     * Sets the position of player to spawn position.
	 * -------------------------------------------------------------------------
	 */
    private void setPlayerToStartPosition(Player p) {
        //and sets creatures to their normal positions
        int x = currentLevel.metadata.startpoint[0]; 
        int y = currentLevel.metadata.startpoint[1];
        p.setX(TileMapRenderer.tilesToPixels(x));
        p.setY(TileMapRenderer.tilesToPixels(y));
        currentLevel.setPlayer(playerSprite);
    }
    	
	/* -------------------------------------------------------------------------
	 * Loads the next map from the levelManager
	 * -------------------------------------------------------------------------
	 */
    public Level loadNextMap() {
        //sets the new tile map 
        currentLevel = levelManager.loadNextLevel();
        if(currentLevel == null) return null;
        //and sets creatures to their normal positions
        setPlayerToStartPosition(playerSprite);
        addGoalSprite(currentLevel);
        //load new background
        //(background is loaded in the level)

        //currentLevel.tilemap.physicsWorld.addGravityToAll(300.0f);
        //return currentLevel.tilemap;

        currentLevel.physicsWorld.addGravityToAll(350.0f);
        System.out.println("This level: "+currentLevel.metadata.lvlName);
        return currentLevel;
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    public Level reloadMap() {
        currentLevel = levelManager.reloadLevel();
        //and sets creatures to their normal positions
        setPlayerToStartPosition(playerSprite);
        addGoalSprite(currentLevel);
        //load new background
        //(background is loaded in the level)
        currentLevel.physicsWorld.addGravityToAll(350.0f);
        return currentLevel;
    }
	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    private void addSprite(Level map, Sprite hostSprite,int tileX,int tileY) {
        if (hostSprite != null) {
            // clone the sprite from the "host"
            Sprite sprite = (Sprite)hostSprite.clone();
            // center the sprite
            sprite.setX(16 +
                TileMapRenderer.tilesToPixels(tileX) +
                (TileMapRenderer.tilesToPixels(1) -
                sprite.getWidth()) / 2);

            // bottom-justify the sprite
            sprite.setY(16 + TileMapRenderer.tilesToPixels(tileY + 1)-sprite.getHeight());

            // add it to the map
            map.addSprite(sprite);
			//System.out.print();
        }
    }
	
	/* -------------------------------------------------------------------------
	 * Code for loading sprites and images This will be removed when new tiles
	 * for enemies are added, this function will then be performed in the
	 * levelManager
	 * -------------------------------------------------------------------------
	 */
    public void loadTileImages() {
        // keep looking for tile A,B,C, etc. this makes it
        // easy to drop new tiles in the images/ directory
        tiles = new ArrayList();
        char ch = 'A';
        while (true) {
            String name = "tile_" + ch + ".png";
            File file = new File("images/" + name);
            if (!file.exists()) {
                break;
            }
            tiles.add(loadTileSizedImage("images/" +name));
            ch++;
        }
    }
	
	/* -------------------------------------------------------------------------
	 * This will be removed when new tiles for enemies are added,
	 * this function will then be performed in the levelManager
	 * -------------------------------------------------------------------------
	 */
    public void loadCreatureSprites() {

        Image[][] images = new Image[4][];
        // ÄNDRAT
        int width = 32;
        int height = 32;
        int rows = 4;
        int cols = 4;
        //load the hero
        final String resourcePath = "materials/characters/" + "spriteSheet3.png";
        Image[] ss = loadSpriteSheet(resourcePath, rows, cols, width, height);

        images[0] = new Image[] {
            ss[0],
            ss[1],
            ss[2],
            ss[3],
            ss[4],
            ss[5],
            ss[6],
            ss[7]
        };
		
        images[1] = new Image[images[0].length];
        images[2] = new Image[images[0].length];
        images[3] = new Image[images[0].length];
        for (int i=0; i<images[0].length; i++) {
            // right-facing images
            images[1][i] = getMirrorImage(images[0][i]);
            // left-facing "dead" images
            images[2][i] = getFlippedImage(images[0][i]);
            // right-facing "dead" images
            images[3][i] = getFlippedImage(images[1][i]);
        }

        // create creature animations
        Animation[] playerAnim = new Animation[4];
        for (int i=0; i<4; i++) {
            playerAnim[i] = createPlayerAnim(
                images[i][0], images[i][1], images[i][2], images[i][3],
                images[i][4], images[i][5], images[i][6], images[i][7]);
        }

        // create creature sprites
        playerSprite = new Player(playerAnim[0], playerAnim[1],
            playerAnim[2], playerAnim[3]);

    }
	
	/* -------------------------------------------------------------------------
	 * ÄNDRAT: This will be removed when new tiles for player are added,
	 * this function will then be performed in the levelManager
	 * -------------------------------------------------------------------------
	 */
    private Animation createPlayerAnim(	Image player1, Image player2,
										Image player3, Image player4,
										Image player5, Image player6,
										Image player7, Image player8) {
        //ÄNDRAT
        Animation anim = new Animation();
        anim.addFrame(player1, 150); anim.addFrame(player2, 150);
        anim.addFrame(player3, 150); anim.addFrame(player4, 150);
        anim.addFrame(player5, 150); anim.addFrame(player6, 150);
        anim.addFrame(player7, 150); anim.addFrame(player8, 150);
        return anim;
    }
	
	/* -------------------------------------------------------------------------
	 * This will be removed when new tiles for enemies are added,
	 * this function will then be performed in the levelManager
	 * -------------------------------------------------------------------------
	 */
    private void loadPowerUpSprites() {
        // create "goal" sprite
        Animation anim = new Animation();
        int width = 32;
        int height = 64;
        int rows = 1;
        int cols = 3;
        final String resourcePath = "materials/characters/" + "princess.png";
        Image[] ss = loadSpriteSheet(resourcePath, rows, cols, width, height);

        anim.addFrame(ss[0], 500);
        anim.addFrame(ss[1], 500);
        anim.addFrame(ss[2], 500);
        anim.addFrame(ss[1], 500);
        goalSprite = new PowerUp.Goal(anim);
    }
    
}
