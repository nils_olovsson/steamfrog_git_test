/**
    The TileMapRenderer class draws a Level on the screen.
    It draws all tiles, sprites, and an optional background image
    centered around the position of the player.

    <p>If the width of background image is smaller the width of
    the tile map, the background image will appear to move
    slowly, creating a parallax background effect.

    <p>Also, three static methods are provided to convert pixels
    to tile positions, and vice-versa.

    <p>This TileMapRender is supposed to use a tile size of 32x32.
*/

package Game;

import Game.Sprites.Sprite;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.util.Iterator;

public class TileMapRenderer {
	
    // the size in bits of the tile
    // Math.pow(2, TILE_SIZE_BITS) == TILE_SIZE
    private static final int TILE_SIZE_BITS = 5;
    public static final int TILE_SIZE = (int)(Math.pow(2,TILE_SIZE_BITS));
     
    private Image background, foreground;
	
	/* -------------------------------------------------------------------------
	 * Converts a pixel position to a tile position.
	 * -------------------------------------------------------------------------
	 */
    public static int pixelsToTiles(float pixels) {
		return pixelsToTiles(Math.round(pixels));
	}
	
    /* -------------------------------------------------------------------------
	 * Converts a pixel position to a tile position and tile to pixel
	 * -------------------------------------------------------------------------
     */
    public static int pixelsToTiles(int pixels) {
		return pixels >> TILE_SIZE_BITS;
    }
	
    public static int tilesToPixels(int numTiles) {
		return numTiles << TILE_SIZE_BITS;
	}
	
	/* -------------------------------------------------------------------------
	 * Draws the specified Level. That is, all sprites in that Level are
	 * rendered on screen. Uses transform offset when rendering.
	 * -------------------------------------------------------------------------
	 */
    public void draw(	Graphics2D g, Level map,
						int screenWidth, int screenHeight) {
		// ----------------------------------------------------------
		// Create transform and set it to standard window coordinates
		// Then load player sprite and get size of map.
		// ----------------------------------------------------------
		AffineTransform transform = g.getTransform();
		//transform.setToIdentity();
		//g.setTransform(transform);
		
        Sprite player	= map.getPlayer();
        int mapWidth	= tilesToPixels(map.getWidth());
		int mapHeight	= tilesToPixels(map.getHeight());
		
		// ----------------------------------------------------------
        // Get scrolling position of based on player's position
		// ----------------------------------------------------------
        int offsetX = screenWidth / 2 - Math.round(player.getX());
        offsetX = Math.min(offsetX, 0);
        offsetX = Math.max(offsetX, screenWidth - mapWidth);
		
        int offsetY = screenHeight/2 - Math.round(player.getY());
		
		// ----------------------------------------------------------
		// Clear screen, load background and then draw background
		// ----------------------------------------------------------
		g.setColor(Color.black);
		g.fillRect(0, 0, screenWidth, screenHeight);
		background = map.getBackground();
		drawBackground(g, offsetX, screenWidth, map);
		
		// ----------------------------------------------------------
		// Set transform to draw everything with translation offset
		// Then render all sprites, last render the player
		// ----------------------------------------------------------
		transform.translate(offsetX, offsetY);
		g.setTransform(transform);
		
		int rad,x,y;//counter=0;
		
        Iterator i = map.getSprites();
        while (i.hasNext()) {
			Sprite sprite = (Sprite)i.next();
			
			rad=sprite.getBoundingRadius();
			x = (int)sprite.getX();
			y = (int)sprite.getY();
			if( (x-rad>=-offsetX-64 && x+rad<=-offsetX+640+128) &&
				(y+rad>=-offsetY-64 && y+rad<=-offsetY+360+128)) {sprite.render(g);}
			//else{counter++;}
			
			//sprite.render(g);
		}
		player.render(g);
		//System.out.println("Nr of sprites not rendered:" + counter);
		// ----------------------------------------------------------
		// Here is some rendering calls to physics engine, for debug
		// ----------------------------------------------------------
		//map.physicsWorld.renderForceGenerators(g);
		//map.physicsWorld.renderContactGeom(g);
		//map.physicsWorld.renderContacts(g);
		//map.physicsWorld.renderBoundingGeom(g);
		
		// ----------------------------------------------------------
		// Set transform to standard window coord and draw foreground
		// ----------------------------------------------------------
		transform.setToIdentity();
		g.setTransform(transform);
		foreground = map.getForeground();
		if(foreground != null){ g.drawImage(foreground, 0, 0, null);}
		
		// ----------------------------------------------------------
		// Draw some text in standard window coordinates.
		// Most (all?) will be removed later. Mostly for debug.
		// ----------------------------------------------------------
		//g.setColor(Color.WHITE);
		//g.drawString("Press 'P' to play", 10, 20);
		//g.drawString("Press 'N' to load skip level", 10, 40);
		//g.drawString(String.format( "Nr of bodies: %d",map.physicsWorld.getNrOfBodies()), 10, 80);
		//player.body.drawInfo(g, 10, 80);
		
    } // End of draw()
	
	/* -------------------------------------------------------------------------
	 * Render background on screen.
	 * -------------------------------------------------------------------------
	 */
	private void drawBackground(Graphics2D g2d, int offsetX,
								int screenWidth, Level map){
		int mapWidth	= tilesToPixels(map.getWidth());
		int x, nominator;
		
		// Draw parallax background (in window coordinates scaled with offset..)
        if (background != null) {
			nominator = screenWidth - mapWidth;
			if(nominator==0){nominator++;}
			x =	(screenWidth - background.getWidth(null))/nominator*offsetX;
			g2d.drawImage(background, x, 0, null);
        }
	}
}
