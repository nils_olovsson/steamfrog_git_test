/*
 /* 
   if(s instanceof SpringElevator){
    SpringElevator se = (SpringElevator)s;
    level.physicsWorld.addForce(se.getBungeeRope(),s.getBody());
   }
 */

package Game;

import Game.Sprites.*;
import Game.Sprites.StaticGameProp.BGDetail;
import Game.Sprites.StaticGameProp.BGTile;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URISyntaxException;
import java.util.*;

public class LevelManager {

    
    Map<String,BufferedImage> imageMap;
    public final String BGTILES_PATH  =   "materials/bgtiles"; 
    public final String BGIMAGES_PATH  =  "materials/bgimages";
    public final String BGDETAILS_PATH  = "materials/bgdetails";
    public final String SPRITE_PATH  = "materials/triggers";
    public final String ENTITIES_PATH = "materials/entities";
    public final String FILTERS_PATH  = "materials/filters";
    public final String LEVELS_PATH  = "levels";
    List<File> levelfiles;
    List<Level> levels;
    Level currentLevel;
    int currentLevelPointer = -1;
    
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
    public LevelManager(){
        imageMap = new HashMap<String,BufferedImage>();
        levels = new ArrayList<Level>();
        levelfiles = new ArrayList<File>();
        try {
            loadImagesToMap(imageMap,BGTILES_PATH);
            loadImagesToMap(imageMap,BGIMAGES_PATH);
            loadImagesToMap(imageMap,BGDETAILS_PATH);
        } catch (URISyntaxException ex) {
            System.out.println("Error when trying to initiate levels!");
            ex.printStackTrace();
        }
    }

	/* -------------------------------------------------------------------------
	 * This method is called from the constructor. It 
     * searches for all the text files in the LEVEL_PATH 
     * and creates levels from them, adding them to a list for 
     * future use. It loads all availible levels one time and also saves 
     * the path to the level file. If a level must be reloaded, the 
     * path is used and the file has to be read again
	 * -------------------------------------------------------------------------
	 */
    public void initiateAllLevels(){
        try{
            File dir = new File(LEVELS_PATH);
            //Get all the files in a specific path
            File[] files = dir.listFiles();
            java.util.Arrays.sort(files);
            System.out.print("LEVELS: ");
            for(File f: files){
                if(!f.getName().endsWith(".txt")){
                    System.out.println(f.getName() +" is no text file!");
                    continue;
                }
                System.out.println(f.getName()+" ");
                String source = LEVELS_PATH+"/"+f.getName();
                
                File levelfile  = new File(source);
                Level l = loadLevel(levelfile);
                
                System.out.println();
                levelfiles.add(levelfile);
                levels.add(l);
            }
            System.out.println();
        }
        catch (Exception ex) {
            System.out.println("Problems trying to load levels");
            ex.printStackTrace();
        }
        if(levels.size()==0){
            System.out.println("Sorry, no level files found!");
        }
    }
    
	/* -------------------------------------------------------------------------
	 * Takes a File and retruns a level
	 * -------------------------------------------------------------------------
	 */
    public Level loadLevel(File file){
        LevelMetadata metadata = new LevelMetadata();
        String lvlName = "";
        int[] dimension = new int[2];
        String bgimage = "background1.png";
        LinkedList<BGTile> tileList = new LinkedList<BGTile>();
        LinkedList<BGDetail> detailList = new LinkedList<BGDetail>();
        LinkedList<Platform> platformList = new LinkedList<Platform>();
        LinkedList<Sprite> nonstaticDetailSprites = new LinkedList<Sprite>();
        //Add entities
        int[] spawnPoint = {-1,-1};
        int[] finishPoint = {-1,-1};
        String[] music = {"","",""};
        Level level;
        String filter = "";
        int[] outerTilesX = {Integer.MAX_VALUE,0}; //used to automatically calculate spawn posisions if there is none specified
        int[] outerPlatformsX = {Integer.MAX_VALUE,0};//used insted of outerTilesX if platforms are specified
        
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while (true) {
                String line = reader.readLine();
                // no more lines to read
                if (line == null) {
                    reader.close();
                    break;
                }
                if (line.startsWith("#")){ continue; }

                else if(line.startsWith("<lvlName>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</lvlName>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            if(!content.equalsIgnoreCase("none")){
                                lvlName = content;
                            }
                        }
                        line = reader.readLine();
                    }
                }
                else if(line.startsWith("<dimensions>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</dimensions>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            int comma = content.indexOf(',');
                            int x = Integer.parseInt(content.substring(0,comma).trim());
                            int y = Integer.parseInt(content.substring(comma+1).trim());
                            dimension[0] = x;
                            dimension[1] = y;
                        }
                        line = reader.readLine();
                    }
                }
                else if(line.startsWith("<bgImage>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</bgImage>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(!content.equalsIgnoreCase("none")){
                                bgimage = content;
                            }
                        line = reader.readLine();
                    }
                }
                else if(line.startsWith("<bgTiles>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</bgTiles>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            int x=0, y=0;
                            int comma1 = content.indexOf(',');
                            int comma2 = content.indexOf(',',comma1+1);
                            try{
                                String img = BGTILES_PATH+"/"+content.substring(0,comma1).trim();
                                x = Integer.parseInt(content.substring(comma1+1,comma2).trim());
                                y = Integer.parseInt(content.substring(comma2+1).trim());
                                if(x<outerTilesX[0]) outerTilesX[0]=x;
                                if(x>outerTilesX[1]) outerTilesX[1]=x;
                                BGTile t = new BGTile(x, y, imageMap.get(img));
                                tileList.add(t);
                            }catch(Exception e){
                                System.err.println("Content = "+content);
                                System.err.println("x = "+x);
                                System.err.println("y = "+y);
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else if(line.startsWith("<bgDetails>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</bgDetails>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            int x=0, y=0;
                            int comma1 = content.indexOf(',');
                            int comma2 = content.indexOf(',',comma1+1);
                            try{
                                String img = BGDETAILS_PATH+"/"+content.substring(0,comma1).trim();
                                x = Integer.parseInt(content.substring(comma1+1,comma2).trim());
                                y = Integer.parseInt(content.substring(comma2+1).trim());

                                BGDetail t = new BGDetail(x, y, imageMap.get(img));
                                detailList.add(t);
                            }catch(Exception e){
                                System.err.println("Content = "+content);
                                System.err.println("x = "+x);
                                System.err.println("y = "+y);
                                e.printStackTrace();
                            }
                        }  
                    }
                }
                else if(line.startsWith("<platforms>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</platforms>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            String type;
                            int x=0, y=0;
                            int comma1 = content.indexOf(',');
                            int comma2 = content.indexOf(',',comma1+1);
                            try{
                                type = content.substring(0,comma1).trim();
                                x    = Integer.parseInt(content.substring(comma1+1,comma2).trim());
                                y    = Integer.parseInt(content.substring(comma2+1).trim());
                                //updates the extreme points
                                if(x<outerPlatformsX[0]) outerPlatformsX[0]=x;
                                if(x>outerPlatformsX[1]) outerPlatformsX[1]=x;
                                Platform p = new Platform(x, y, type);
                                platformList.add(p);
                            }catch(Exception e){
                                System.err.println("Error when reading platform");
                                System.err.println("Content = "+content);
                                System.err.println("x,y = "+x+","+y);
                                e.printStackTrace();
                            }
                        }
                    }
                }
                else if(line.startsWith("<entities>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</entities>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content==null) break;
                        String[] parts = content.split(",");
                        String type = parts[0];
                        String imgPath = ENTITIES_PATH+"/";//Var ska dessa ligga?
                        //For the sake of backward compability, the line could have the form "{type,img,x,y} instead of {type,x,y}"
                        //but a warning will be printed and the image ignored
						int x = Integer.parseInt(parts[1].trim());
						int y = Integer.parseInt(parts[2].trim());
						
                        if(type.toLowerCase().startsWith("cog")) {
                            imgPath+="cog.png";
                            Image img = ResourceManager.loadImage(imgPath);
                            Gear g = new Gear(img,x,y);
                            nonstaticDetailSprites.add(g);
                        }
                        else if(type.toLowerCase().startsWith("springelevator")){ 
							imgPath+="springElevator.png";
                            Image img = ResourceManager.loadImage(imgPath);
                            SpringElevator s = new SpringElevator(img,x+1,y);
                            nonstaticDetailSprites.add(s);
                        }
                        else if(type.toLowerCase().startsWith("crate")){ //
                            imgPath+="crate.png";
                            Image img = ResourceManager.loadImage(imgPath);
                            Crate c = new Crate(img,x,y);
                            nonstaticDetailSprites.add(c);
                        }
                        else{
                            System.out.println("unknown entity: "+type);
                        }
                    }
                }
                else if(line.startsWith("<triggers>")){ //ie <triggers><spawn-point>{1,2}</spawn-point></triggers>
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</triggers>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content==null){
                            break;
                        }
                        if(content.startsWith("spawn")){ 
                            //read until the endtag is found    
                            int comma1 = content.indexOf(',');
                            int comma2 = content.indexOf(',',comma1+1);
                            int x = Integer.parseInt(content.substring(comma1+1,comma2).trim());
                            int y = Integer.parseInt(content.substring(comma2+1).trim());
                            spawnPoint[0] = x;
                            spawnPoint[1] = y;
                        }  
                        else if(content.startsWith("finish")){ 
                            //read until the endtag is found    
                            int comma1 = content.indexOf(',');
                            int comma2 = content.indexOf(',',comma1+1);
                            int x = Integer.parseInt(content.substring(comma1+1,comma2).trim());
                            int y = Integer.parseInt(content.substring(comma2+1).trim());
                            finishPoint[0] = x;
                            finishPoint[1] = y;
                        }
                        
                        else{
                            System.out.println("Unknown triggers-tag: " +line);
                        }
                        
                    }//end of while read triggers
                }//end-of-triggers
                else if(line.startsWith("<music>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</music>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null){
                            content = content.replaceAll("\\s","");
                            music = content.split(",");
                        }
                        line = reader.readLine();
                    }
                }
                else if(line.startsWith("<filter>")){
                    //read until the endtag is found
                    while(line!=null && !line.startsWith("</filter>")){
                        line = reader.readLine();
                        String content = getContent(line);
                        if(content != null && !content.equalsIgnoreCase("none")){
                            filter = content.trim();
                        }
                        line = reader.readLine();
                    }
                }
                else if(line.length()==0){
                    continue;
                }
                else{
                    System.out.println("Unknown tag: "+line);
                }
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        //Reading done, adding static sprites to level
        level = new Level(dimension[0],dimension[1]);
        for(BGTile t: tileList){
            level.addSprite(t);
        }
        for(BGDetail t: detailList){
            level.addSprite(t);
        }
        
        //adding nonstatic detail sprites
        for(Sprite s: nonstaticDetailSprites){
            
			if(s instanceof SpringElevator){
				SpringElevator se = (SpringElevator)s;
				level.physicsWorld.addForce(se.getBungeeRope(),s.getBody());
			}
            level.addSprite(s);
        }
        
        //Builds platforms automatic at tile positions if exactly zero 
        //platforms were found in the level file
		if(platformList.size()==0){
            System.out.println("No platforms found, builds default");
            buildPlatforms(tileList,level);
        }else{
            for(Platform p: platformList){
                level.addSprite(p);
            }
        }
        
        //if no spawn-point was specified, calculate it
        if(spawnPoint[0]==-1){
            spawnPoint[0]=Math.min(outerPlatformsX[0],outerTilesX[0]);
            spawnPoint[1]=0;
        } 
        //if no goal point was specified, calculate it
        if(finishPoint[0]==-1){
            finishPoint[0]=Math.max(outerPlatformsX[1],outerTilesX[1]);
            finishPoint[1]=0;
        }
        
        
        System.out.println("platforms "+platformList.size());
        
        
//		buildStaticProps(tilemap);
        
        //Sets the metadata, like name
        metadata.lvlName = lvlName;
        metadata.dimension = dimension;
        if(filter!=null && filter.length()>0) metadata.foreground = FILTERS_PATH+"/"+ filter;
        metadata.background = BGIMAGES_PATH+"/"+ bgimage;  
        metadata.startpoint = spawnPoint;
        metadata.goal = finishPoint;
        metadata.music = music;
        
        level.metadata = metadata;
        System.out.println("===========");
        System.out.println(metadata);
        System.out.println("===========");
        
        createBackground(metadata.background,level);
        createForeground(metadata.foreground,level);
        
        // add the player to the map
        // (in resourceManager)
         
        return level;
        
    }
	
    
	/* -------------------------------------------------------------------------
	 * Get the content from a string that is inside {}
	 * -------------------------------------------------------------------------
	 */
    private String getContent(String line) {
        int startwing	= line.indexOf('{');
        int endwing		= line.indexOf('}',startwing);
		
		if(startwing == -1){ return null;}
        return line.substring(startwing+1, endwing);
	}
	
	/* -------------------------------------------------------------------------
	 * Retruns a copy of the next level from the precreated list of levels, 
     * if there is no more levels null is returned
	 * -------------------------------------------------------------------------
	 */
    public Level loadNextLevel(){
        currentLevelPointer++;
		currentLevelPointer%=levels.size();
        //if(currentLevelPointer==levels.size()){
        //    System.out.println("No more levels!");
        //    return null;
        //}
        currentLevel = reloadLevel(currentLevelPointer);//levels.get(currentLevelPointer);
        System.out.println("level: "+currentLevel.metadata.lvlName + " "+(currentLevelPointer+1) +"/" +levels.size());
        
        return currentLevel;
    }
   
    /* -------------------------------------------------------------------------
	 * Retruns a new copy of the current level, by reading the levelFile again
	 * -------------------------------------------------------------------------
	 */
    public Level reloadLevel(){
        return reloadLevel(currentLevelPointer);
    }
    
    /* -------------------------------------------------------------------------
	 * Return a new copy of a level with a specific number
	 * -------------------------------------------------------------------------
	 */
    public Level reloadLevel(int i){
        Level level = loadLevel(levelfiles.get(i));
        return level;
    }

    
     	
	/* -------------------------------------------------------------------------
	 * 
	 * -------------------------------------------------------------------------
	 */
    private void loadImagesToMap(Map<String,BufferedImage> map, String PATH)
													throws URISyntaxException {
		//This code will not work form a jar-file unfortunatly how to fix that?
        File dir = new File(PATH);
        
		//Get all the files in a specific path
        File[] files = dir.listFiles();
        for(File f: files){
			String source = PATH+"/"+f.getName();
			map.put(source,ResourceManager.loadTileSizedImage(source));
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Create and add Platforms at positions of tiles
	 * -------------------------------------------------------------------------
	 */
	public void buildPlatforms(LinkedList<BGTile> tiles,Level tilemap) {
		
		for(BGTile tile: tiles) {
            float x = tile.getX();
            float y = tile.getY();
			Platform p = new Platform((int)x,(int)y,0);
            p.setX(x);
            p.setY(y);
            tilemap.addSprite(p);
		}	

	}
	
	/* -------------------------------------------------------------------------
	 * Create and add StaticGameProps at position of tiles
	 * -------------------------------------------------------------------------
	 */
	public void buildStaticProps(Level map) {}

    /* -------------------------------------------------------------------------
	 * sets the background of the level file to a BufferedImage based on 
     * the path-string in the first argument
	 * -------------------------------------------------------------------------
	 */
    public void createBackground(String background, Level level){
		//sets the background
		int gameWidth  = level.getWidth()*TileMapRenderer.TILE_SIZE;
		int gameHeight = level.getHeight()*TileMapRenderer.TILE_SIZE;      

		BufferedImage b =	ResourceManager.createBackgroundImage
							(background,gameWidth, gameHeight);
		level.setBackground(b);
    }
    
    
    /* -------------------------------------------------------------------------
	 * sets the foreground of the level file to a BufferedImage  based on 
     * the path-string in the first argument
	 * -------------------------------------------------------------------------
	 */
    public void createForeground(String foreground, Level level){
        if(foreground==null || foreground.equals(""))return;
		//sets the background
		int gameWidth  = level.getWidth()*TileMapRenderer.TILE_SIZE;
		int gameHeight = level.getHeight()*TileMapRenderer.TILE_SIZE;      

		BufferedImage f =	ResourceManager.loadImageOfSize
							(foreground,gameWidth, gameHeight);
		level.setForeground(f);
    }
    
    
    
    
    
}
