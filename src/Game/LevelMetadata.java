package Game;

import java.awt.image.BufferedImage;

public class LevelMetadata {
    
	String lvlName;
	int[] dimension = new int[2];
	String background;
    String foreground;
	Level tilemap;

	//Triggers
	int[] goal;
	int[] startpoint;
	String music[]; //intro, loop, outro
	
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
    public LevelMetadata() {}
	
    public LevelMetadata(	String name, int[] dim, String background, Level t,
						int[] start, int[] goal, String[] music) {
		lvlName = name;				dimension = dim;	tilemap = t;
		this.startpoint = start;	this.goal = goal;	this.music = music;
	}
	
    
	/* -------------------------------------------------------------------------
	 * returns a formatted informative string with multiple lines
	 * -------------------------------------------------------------------------
	 */
    public String toString(){
        return String.format(
		"  %15s%s\n  %15s%s*%s\n  %15s%s\n"
      + "  %15s%s\n  %15s%s,%s\n  %15s%s,%s\n  %15s%s\n ", 
		
      "Level name: ", lvlName,
		"Dimensions: ", dimension[0], dimension[1],
		"Background: ",background,
        "Foreground: ",foreground,
		"Spawn-point: ",startpoint[0],startpoint[1],
		"Finish-point: ",goal[0],goal[1],
		"Music: ",java.util.Arrays.toString(music)+"(size="+music.length+")");
	}
    
}