 /**
  * GameManager manages most parts of the game...
  * 
  */

package Game;

import Game.Sprites.Creature;
import Game.Sprites.Player;
import Game.Sprites.Sprite;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

import physics.RigidPhysics.NarrowCollision.Contact;
import physics.RigidPhysics.RigidBody;
import soundengine.Music;

public class GameManager {
	
	// Here are instance variables from core
	protected JFrame			frameScreen;
	protected Canvas			canvas;
	protected BufferStrategy	buffer;
	protected BufferedImage		bi;
	protected final int			SCREEN_WIDTH	= 640;	// Used for rendering in
	protected final int			SCREEN_HEIGHT	= 360;	// windowed mode
	
	protected Image pauseScreen;
	protected Image titleScreen;
	private String PS_PATH = "materials/bgimages";
	private String TS_PATH = "materials/bgimages";
	
	private boolean isRunning;
	protected boolean paused;
	
	// Here are instance variables from manager
	private ResourceManager resourceManager;
	private Input inputManager;
	private Level map;
	private TileMapRenderer renderer;
	private Music song = new Music(	"materials/music/intro.wav",
									"materials/music/loop.wav", -5);

	private GameAction moveLeft, moveRight, jump, grab, exit, reload, pause, skipLevel;
	
	/* -------------------------------------------------------------------------
	 * Main() and run(). Game is launched from here.
	 * -------------------------------------------------------------------------
	 */
    public static void main(String[] args) {new GameManager().run();}
	public void		   run() {try {init(); gameLoop();} finally {lazilyExit();}}
	
	/* -------------------------------------------------------------------------
	 * Initialize
	 * -------------------------------------------------------------------------
	 */
    public void init() {
		initGraphics();
		isRunning	= true;
		paused		= true;
		pauseScreen = loadImage("materials/menu/SimplePause.png");
		titleScreen = loadImage("materials/menu/SimpleTitle.png");
		
        initInput();	// set up input manager

        // start resource manager
        resourceManager = new ResourceManager(getGraphicsConfiguration());
        map = resourceManager.loadNextMap();// load first map
        renderer = new TileMapRenderer();	// Initialize rendering of map
    }
	
	/* -------------------------------------------------------------------------
	 * Initializes a JFrame etc
	 * -------------------------------------------------------------------------
	 */
	public void initGraphics() {
			// Create game window...
			frameScreen = new JFrame();
			frameScreen.setIgnoreRepaint( true );
			frameScreen.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
			frameScreen.setTitle("Steam Frog!");
			frameScreen.setBackground(Color.BLACK);
			
			// Create canvas for painting...
			canvas = new Canvas();
			canvas.setIgnoreRepaint( true );
			canvas.setSize( SCREEN_WIDTH, SCREEN_HEIGHT);
			canvas.setBackground(Color.BLACK);

			// Add canvas to game window...
			frameScreen.add( canvas );
			frameScreen.pack();
			frameScreen.setVisible( true );

			// Create BackBuffer...
			canvas.createBufferStrategy( 2 );
			buffer = canvas.getBufferStrategy();

			// Get graphics configuration...
			GraphicsEnvironment ge = 
				GraphicsEnvironment.getLocalGraphicsEnvironment();
			GraphicsDevice gd = ge.getDefaultScreenDevice();
			GraphicsConfiguration gc = gd.getDefaultConfiguration();

			// Create off-screen drawing surface
			bi = gc.createCompatibleImage(SCREEN_WIDTH,SCREEN_HEIGHT);
			
	} // End of initGraphics()
	
	/* -------------------------------------------------------------------------
	 * Exits the VM from a daemon thread. The daemon thread waits 2 seconds then
	 * calls System.exit(0). Since the VM should exit when only daemon threads
	 * are running, this makes sure System.exit(0) is only called if neccesary.
	 * It's neccesary if the Java Sound system is running.
	 * -------------------------------------------------------------------------
	 */
    public void lazilyExit() {
		Thread thread = new Thread() {
			public void run() {
				try {Thread.sleep(100);
				}catch (InterruptedException ex) { } System.exit(0);
		}};
		thread.setDaemon(true); thread.start();
    }
	
	/* -------------------------------------------------------------------------
	 * Smaller methods
	 * -------------------------------------------------------------------------
	 */
	public GraphicsConfiguration getGraphicsConfiguration() {
		return bi.createGraphics().getDeviceConfiguration();
	}
	public Component getComponent()	{return frameScreen;}
	public int getScreenWidth()		{return SCREEN_WIDTH;}
	public int getScreenHeight()	{return SCREEN_HEIGHT;}
	public Level getMap()			{return map;}
	public boolean isPaused()		{return paused;}
	public void stop()				{isRunning = false;} // Exit gameloop

	public void setPaused(boolean p) {
		if (paused != p) {this.paused = p; inputManager.resetAllGameActions();}
	}
	public Image loadImage(String fileName) {
        return new ImageIcon(fileName).getImage();
    }
	
	/* -------------------------------------------------------------------------
	 * Initialize input
	 * -------------------------------------------------------------------------
	 */
    private void initInput() {
		moveLeft = new GameAction("moveLeft");
		moveRight = new GameAction("moveRight");
		grab = new GameAction("grab",
				GameAction.DETECT_INITAL_PRESS_ONLY);
		jump = new GameAction("jump",
				GameAction.DETECT_INITAL_PRESS_ONLY);
		exit = new GameAction("exit",
				GameAction.DETECT_INITAL_PRESS_ONLY);
		reload = new GameAction("reload",
				GameAction.DETECT_INITAL_PRESS_ONLY);
		pause = new GameAction("pause",
				GameAction.DETECT_INITAL_PRESS_ONLY);
		skipLevel = new GameAction("skipLevel",
				GameAction.DETECT_INITAL_PRESS_ONLY);

		// We will not be using a cursor...
		inputManager = new Input(getComponent());
		//inputManager.setCursor(Input.INVISIBLE_CURSOR);
		
		inputManager.mapToKey(moveLeft, KeyEvent.VK_LEFT);
		inputManager.mapToKey(moveRight, KeyEvent.VK_RIGHT);
		inputManager.mapToKey(grab, KeyEvent.VK_CONTROL);
		inputManager.mapToKey(jump, KeyEvent.VK_SPACE);
		inputManager.mapToKey(jump, KeyEvent.VK_UP);
		inputManager.mapToKey(exit, KeyEvent.VK_ESCAPE);
		inputManager.mapToKey(pause, KeyEvent.VK_P);
		inputManager.mapToKey(reload, KeyEvent.VK_R);
		inputManager.mapToKey(skipLevel, KeyEvent.VK_N);
    }
	
	/* -------------------------------------------------------------------------
	 * This is the gameloop
	 * -------------------------------------------------------------------------
	 */
    public void gameLoop() {
		
		// ----------------------------------------------------------
		// Initialize time measurment and starts a song
		// ----------------------------------------------------------
        long currTime  = 0;
		song.start();
        while (isRunning) {
			
			// Measure time (IN NANO SECONDS!!!!!!!!!!!!!!!)
            long elapsedTime = System.nanoTime() - currTime;
            currTime += elapsedTime;
			
			// Get keyboard/mouse input
			checkInput(elapsedTime/1000);
			
            // Update game
            update(elapsedTime);
			
			// ------------------------------------------------------
			// Render in a window
			// ------------------------------------------------------
			Graphics2D g;
			g = bi.createGraphics();
			renderer.draw(g, map, getScreenWidth(), getScreenHeight());
			//g.drawString(String.format("Update Time:"+elapsedTime/1000000+"[ms]"), 10, 60);
			if(paused) {g.drawImage(pauseScreen, 0,0,null);}
			
			// ------------------------------------------------------
			// Calculate the scaling of final image and apply it.
			// Then blit image and flip...
			// ------------------------------------------------------
			Graphics2D g2 = (Graphics2D)buffer.getDrawGraphics();
			
			int width = canvas.getWidth();
			int height = canvas.getHeight();
			double scaleFactor=1.0;
			double scaleHor	= (double)width/SCREEN_WIDTH;
			double scaleVer	= (double)height/SCREEN_HEIGHT;
			
			if		(scaleVer<scaleHor) {scaleFactor = scaleVer;}
			else if (scaleVer>scaleHor) {scaleFactor = scaleHor;}
			g2.scale(scaleFactor,scaleFactor);
			
			g2.drawImage( bi, 0, 0, null );
			if( !buffer.contentsLost() )
			buffer.show();
        }
    }
	
	/* -------------------------------------------------------------------------
	 * Check for various kinds of input and set "states" depending on them
	 * -------------------------------------------------------------------------
	 */
    private void checkInput(long elapsedTime) {

        if (exit.isPressed())		{stop();}
        if (pause.isPressed())		{setPaused(!isPaused());}
        if (reload.isPressed())		{map = resourceManager.reloadMap();}
		if (skipLevel.isPressed())	{map=resourceManager.loadNextMap();return;}

        Player player = (Player)map.getPlayer();
		if (player.isAlive()) {
			if (moveLeft.isPressed())	{player.walkL(true);}
			if (moveRight.isPressed())	{player.walkR(true);}
			if (!moveLeft.isPressed() &&
				!moveRight.isPressed()){player.setVelocityX(0.0f);}
			
			if (jump.isPressed())		{player.jump(true);}
			if (grab.isPressed())		{player.grab();}
		}
	}
	
    /* -------------------------------------------------------------------------
	 * Updates Animation, position, and velocity of Sprites in the current map.
	 * -------------------------------------------------------------------------
	 */
    public void update(long elapsedTime) {
		
		Creature player = (Creature)map.getPlayer();

		// player is dead! start map over
		if (player.getState() == Creature.STATE_DEAD) {
			map = resourceManager.reloadMap();
			player.setState(Creature.STATE_NORMAL);
			return;
		}
		
		// Check if we won the level
		Player p = (Player)player;
		if( p.isAtGoal ) {map = resourceManager.loadNextMap();}

		if (!isPaused()) {
			
			// Uppdate physics (physics engine run in seconds)
			float timeInSeconds = (float)elapsedTime/1000000000;
			long  timeInMS	    = elapsedTime/1000000;
			if(timeInSeconds>0.015f) {timeInSeconds = 0.015f;}
			map.physicsWorld.runForces(timeInSeconds);
			map.physicsWorld.runGenerateContacts(timeInSeconds);
			
			// Update player
			player.update(timeInMS);

			// update other sprites
			Iterator i = map.getSprites();
			while (i.hasNext()) {
				Sprite sprite = (Sprite)i.next();
				sprite.update(timeInMS);
			}
			
			// Uppdate game collision events
			gameUpdateCollisions(map.physicsWorld.getContacts());
			map.physicsWorld.runSolveContacts(timeInSeconds);
			
			// Set dead if pos i low, not very nice that it is here. Move it.
			if(player.getY() > TileMapRenderer.tilesToPixels(map.getHeight())) {player.setDead();}
			
		}
    }
	
	/* -------------------------------------------------------------------------
	 * Goes through list of contacts generated this frame and lets the game
	 * have a look at them before they are sent back to the physics engine
	 * for solving. The game may also remove contacts from list if it determines
	 * that it is in no need to be solved further.
	 * Actuall solving is done by the involved sprites themselves.
	 * -------------------------------------------------------------------------
	 */
	public void gameUpdateCollisions(ArrayList contacts){
		int length = contacts.size();
		boolean		remove1, remove2;
		Sprite		sprite1, sprite2;
		Contact		current;
		
		for(int i=0;i<length;i++) {
			// Get the sprites involved in the current collision
			current = (Contact)contacts.get(i);
			sprite1 = map.getSprite(current.rb1);
			sprite2 = map.getSprite(current.rb2);
			
			// Try to solve them with game logic
			remove1 = sprite1.collide(sprite2, current);
			remove2 = sprite2.collide(sprite1, current);
			
			// See if they should be removed from the current contact
			if(remove1){current.rb1 = null;}
			if(remove2){current.rb2 = null;}
		}
	}
}
