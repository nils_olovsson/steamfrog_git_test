package soundengine;

import java.io.File;
import java.io.IOException;
import javax.sound.sampled.*;

public class Sound
{
    private int              soundLength;
    private AudioInputStream sound;
    private Clip             clip;
    private float            volume;
    
    public Sound(String filePath, float volume)
    {
        try
        {
            File soundFile = new File(filePath);
            this.sound     = AudioSystem.getAudioInputStream(soundFile);
            this.volume    = volume;
        }
        catch(IOException e){}
		catch(UnsupportedAudioFileException e){System.out.println("Error loading sound: "+e);}
    }
    public void play()
    {
        play(false);
    }
    public void loop()
    {
        play(true);
    }
    private void play(boolean loop)
    {
        try
        {
            DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
            clip               = (Clip) AudioSystem.getLine(info);
            clip.open(sound);
            FloatControl vol = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            vol.setValue(volume);
            soundLength = (int) (clip.getMicrosecondLength()/1000);
            if(loop)
            {
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
            else
            {
                clip.start();
            }
        }
        catch(LineUnavailableException e){}
		catch(IOException e){System.out.println("Error playing sound: "+ e);}
    }
    public int getSoundLength()
    {
        return soundLength;
    }
    public void stopSound()
    {
        clip.stop();
    }
}