package soundengine;

public class Music extends Thread implements Runnable
{
    private Sound intro;
    private Sound loop;
    
    // Constructor
    public Music(String intro, String loop, float volume)
    {
        this.intro = new Sound(intro, volume);
        this.loop = new Sound(loop, volume);
    }
    // Play the music
    public void play()
    {
        intro.play();
        try
        {
            sleep(intro.getSoundLength());
        }
        catch(InterruptedException e){}
        loop.loop();
        while(true)
        {
            try
            {
                sleep(loop.getSoundLength());
            }
            catch(InterruptedException e){}
        }
    }
    // Stop the song
    public void stopSong()
    {
        stop();
    }
    // Start the music as a thread
    public void run()
    {
        play();
    }
}