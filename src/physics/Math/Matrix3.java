/*
 * Matrix3.java
 * 3x3 matrix class that implements some of the most basic operations and some 
 * special ones needed in the physics engine.
 * 
 * NOTE: Uses float, not double
 * TODO: Nothing! (Maybe clean up a bit...)
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.Math;

public class Matrix3 {
	public float data[];	// Holds nine elements
	
	/* -------------------------------------------------------------------------
	 * Constructors 
	 * -------------------------------------------------------------------------
	 */
	public Matrix3()			{data = new float[9];}
	public Matrix3(Matrix3 m) {
		data = new float[9];
		for(int i=0;i<9;i++){data[i]=m.data[i];};
	}
	public Matrix3(Vector3 f1, Vector3 f2, Vector3 f3)
	{
		data = new float[9];
		data[0] = f1.x; data[1] = f2.x; data[2] = f3.x;
		data[3] = f1.y; data[4] = f2.y; data[5] = f3.y;
		data[6] = f1.z; data[7] = f2.z; data[8] = f3.z;
	}
	public Matrix3(	float a, float b, float c,
					float d, float e, float f,
					float g, float h, float i) {
		data = new float[9];
		data[0] = a; data[1] = b; data[2] = c;
		data[3] = d; data[4] = e; data[5] = f;
		data[6] = g; data[7] = h; data[8] = i;		
	}
	
	/* -------------------------------------------------------------------------
	 * toString
	 * -------------------------------------------------------------------------
	 */
	public String toString() {
		return	"["+data[0]+", "+data[1]+", "+data[2]+"]\n"+
				"["+data[3]+", "+data[4]+", "+data[5]+"]\n"+
				"["+data[6]+", "+data[7]+", "+data[8]+"]\n";
	}
	
	/* -------------------------------------------------------------------------
	 * clear
	 * -------------------------------------------------------------------------
	 */
	public void clear() {data = null;}
	
	/* -------------------------------------------------------------------------
	 * Setters
	 * -------------------------------------------------------------------------
	 */
	public void zero()			{for(int i=0;i<9;i++){data[i] = 0.0f;}}
	public void ones()			{for(int i=0;i<9;i++){data[i] = 1.0f;}}
	public void eye()			{zero(); data[0]=data[4]=data[8]=1.0f;}
	public void set(Matrix3 m)	{for(int i=0;i<9;i++){data[i] = m.data[i];}}
	public void set(float a, float b, float c,
					float d, float e, float f,
					float g, float h, float i) {
		data[0] = a; data[1] = b; data[2] = c;
		data[3] = d; data[4] = e; data[5] = f;
		data[6] = g; data[7] = h; data[8] = i;
	}
	
	public void set(Vector3 f1, Vector3 f2, Vector3 f3)
	{
		data[0] = f1.x; data[1] = f2.x; data[2] = f3.x;
		data[3] = f1.y; data[4] = f2.y; data[5] = f3.y;
		data[6] = f1.z; data[7] = f2.z; data[8] = f3.z;
	}
	
	/* -------------------------------------------------------------------------
	 * Add, sub, mult, div with scalars
	 * -------------------------------------------------------------------------
	 */
	public void add(float f)	{for(int i=0;i<9;i++){data[i] += f;}};
	public void sub(float f)	{for(int i=0;i<9;i++){data[i] -= f;}};
	public void mult(float f)	{for(int i=0;i<9;i++){data[i] *= f;}};
	public void div(float f)	{for(int i=0;i<9;i++){data[i] /= f;}};
	
	public void add(Matrix3 m) {for(int i=0;i<9;i++){data[i] += m.data[i];}}
	public void sub(Matrix3 m) {for(int i=0;i<9;i++){data[i] -= m.data[i];}}
	
	/* -------------------------------------------------------------------------
	 * Matrix Matrix multiplication (cross product)
	 * -------------------------------------------------------------------------
	 */
	public Matrix3 cross(Matrix3 m) {
		Matrix3 A = new Matrix3();
		// Row one
		A.data[0] = data[0]*m.data[0]+data[3]*m.data[1]+data[6]*m.data[2];
		A.data[1] = data[1]*m.data[0]+data[4]*m.data[1]+data[7]*m.data[2];
		A.data[2] = data[2]*m.data[0]+data[5]*m.data[1]+data[8]*m.data[2];
		// Row two
		A.data[3] = data[0]*m.data[3]+data[3]*m.data[4]+data[6]*m.data[5];
		A.data[4] = data[1]*m.data[3]+data[4]*m.data[4]+data[7]*m.data[5];
		A.data[5] = data[2]*m.data[3]+data[5]*m.data[4]+data[8]*m.data[5];
		// Row three
		A.data[6] = data[0]*m.data[6]+data[3]*m.data[7]+data[6]*m.data[8];
		A.data[7] = data[1]*m.data[6]+data[4]*m.data[7]+data[7]*m.data[8];
		A.data[8] = data[2]*m.data[6]+data[5]*m.data[7]+data[8]*m.data[8];
		return A;
	}
	
	/* -------------------------------------------------------------------------
	 * Matrix Vector3 multiplication (cross product)
	 * -------------------------------------------------------------------------
	 */
	public Vector3 cross(Vector3 v) {
		float _x = 	data[0]*v.x + data[1]*v.y + data[2]*v.z;
		float _y = 	data[3]*v.x + data[4]*v.y + data[5]*v.z;
		float _z = 	data[6]*v.x + data[7]*v.y + data[8]*v.z;
		return new Vector3(_x,_y,_z);
	}
	
	/* -------------------------------------------------------------------------
	 * Transform with 2D vectors (cross product, z element is always 1.0f)
	 * -------------------------------------------------------------------------
	 */
	public Vector2 cross(Vector2 v) {
		float _x = data[0]*v.x + data[1]*v.y + data[2];
		float _y = data[3]*v.x + data[4]*v.y + data[5];
		return new Vector2(_x,_y);
	}
	
	public Vector2 trans(Vector2 v) {return cross(v);}
	
	public void transUpdate(Vector2 v) {
		float _x = data[0]*v.x+data[1]*v.y+data[2];
		float _y = data[3]*v.x+data[4]*v.y+data[5];
		v.x = _x;v.y = _y;
	}
	
	public Vector2 invTrans(Vector2 v) {
		Matrix3 inv = this.inverseNew();
		return inv.trans(v);
	}
	
	// No translation required, save some operations...
	public Vector2 transDir(Vector2 v) {
		float _x = data[0]*v.x+data[1]*v.y;
		float _y = data[3]*v.x+data[4]*v.y;
		return new Vector2(_x,_y);
	}
	
	public Vector2 invTransDir(Vector2 v) {
		Matrix3 inv = this.inverseNew();
		return inv.transDir(v);
	}
	
	/* -------------------------------------------------------------------------
	 * Determinant
	 * -------------------------------------------------------------------------
	 */
	public float determinant() {
		return	data[0]*(data[4]*data[8]-data[5]*data[7]) - 
				data[1]*(data[3]*data[8]-data[5]*data[6]) +
				data[2]*(data[3]*data[7]-data[4]*data[6]);
	}
	
	/* -------------------------------------------------------------------------
	 * Transpose
	 * -------------------------------------------------------------------------
	 */
	public void transpose() {
		float buffer[] = new float[9]; 
		for(int i=0;i<9;i++){buffer[i] = data[i];}
		data[1] = buffer[3]; data[2] = buffer[6]; data[5] = buffer[7];
		data[3] = buffer[1]; data[6] = buffer[2]; data[7] = buffer[5];
		buffer = null;
	}
	
	public Matrix3 transposeNew() {
		Matrix3 A = new Matrix3(this); A.transpose(); return A;
	}

	/* -------------------------------------------------------------------------
	 * Inverses
	 * -------------------------------------------------------------------------
	 */	
	public void setInverse(Matrix3 m)
	{
		float buffer[] = new float[9];
		for(int i=0;i<9;i++){buffer[i]=m.data[i];}
		
		float t4	= buffer[0]*buffer[4];
		float t6	= buffer[0]*buffer[5];
		float t8	= buffer[1]*buffer[3];
		float t10	= buffer[2]*buffer[3];
		float t12	= buffer[1]*buffer[6];
		float t14	= buffer[2]*buffer[6];

		// Calculate the determinant
		float t16 = (t4*buffer[8] - t6*buffer[7] - t8*buffer[8]+
					t10*buffer[7] + t12*buffer[5] - t14*buffer[4]);

		// Make sure the determinant is non-zero.
		if (t16 == 0.0f) return;
		float t17 = 1/t16;

		data[0] = (buffer[4]*buffer[8]-buffer[5]*buffer[7])*t17;
		data[1] = -(buffer[1]*buffer[8]-buffer[2]*buffer[7])*t17;
		data[2] = (buffer[1]*buffer[5]-buffer[2]*buffer[4])*t17;
		data[3] = -(buffer[3]*buffer[8]-buffer[5]*buffer[6])*t17;
		data[4] = (buffer[0]*buffer[8]-t14)*t17;
		data[5] = -(t6-t10)*t17;
		data[6] = (buffer[3]*buffer[7]-buffer[4]*buffer[6])*t17;
		data[7] = -(buffer[0]*buffer[7]-t12)*t17;
		data[8] = (t4-t8)*t17;
		
		buffer = null;
	}
	
	public Matrix3 inverseNew() {
		Matrix3 A=new Matrix3(this); A.setInverse();return A;
	}
	
	public void setInverse() {this.setInverse(this);}
	
	public Vector2 getAxisVector(int i) {
		return new Vector2(data[i], data[i+3]);
	}
	
	
	/* -------------------------------------------------------------------------
	 * Sets this matrix to be a skew symmetric version of the given vector.
	 * Vector2 version treats z-elememt as zero.
	 * -------------------------------------------------------------------------
	 */
	public void setSkewSymmetric(Vector3 v) {
		data[0] = 0.0f; data[4] = 0.0f; data[8] = 0.0f;
		data[1] = -v.z;
		data[2] = v.y;
		data[3] = v.z;
		data[5] = -v.x;
		data[6] = -v.y;
		data[7] = v.x;
	}
	
	public void setSkewSymmetric(Vector2 v) {
		data[0] = 0.0f; data[4] = 0.0f; data[8] = 0.0f;
		data[1] = 0.0f;
		data[2] = v.y;
		data[3] = 0.0f;
		data[5] = -v.x;
		data[6] = -v.y;
		data[7] = v.x;
	}
	
	
}

