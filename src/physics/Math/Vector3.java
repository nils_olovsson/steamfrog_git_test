package physics.Math;

/*
 * Vector3.java
 * Basic row vector with three values and some basic operations.
 * 
 * NOTE: Uses float, not double
 * TODO: Nothing!
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

public class Vector3 {
	
	public float x,y,z;
	
	/* Constructors */
	public Vector3() {x=0.0f;y=0.0f;z=0.0f;}
	public Vector3(float _x, float _y, float _z)	 {x=_x;y=_y;z=_z;}
	public Vector3(double _x, double _y, double _z) {x=(float)_x;y=(float)_y;z=(float)_z;}
	public Vector3(Vector3 v) {x=v.x;y=v.y;z=v.z;}
	public Vector3(Vector2 v) {x=v.x;y=v.y;z=0.0f;}
	
		/* toString() */
	public String toString() {return "["+x+", "+y+", "+z+"]";}
	
	/* Invert, normalization and magnitude methods */
	public void invert()			{x=-x;y=-y; z=-z;}
	public float magnitude()		{return (float)Math.sqrt(x*x+y*y+z*z);}
	public float sqrdMagnitude()	{return x*x+y*y+z*z;}
	public void normalize() {
		float im = 1/magnitude(); if(im>0){x*=im; y*=im; z*=im;}
	}
	// Is a fast bithack norm possible in java?
	
	/* Methods involving scalars */
	public void add(float f)	{x+=f;y+=f;z+=f;}
	public void sub(float f)	{x-=f;y-=f;z-=f;}
	public void mult(float f)	{x*=f;y*=f;z*=f;}
	public void div(float f)	{x/=f;y/=f;z/=f;}
	
	public void add(double f)	{x+=(float)f;y+=(float)f;z+=(float)f;}
	public void sub(double f)	{x-=(float)f;y-=(float)f;z-=(float)f;}
	public void mult(double f)	{x*=(float)f;y*=(float)f;z*=(float)f;}
	public void div(double f)	{x/=(float)f;y/=(float)f;z/=(float)f;}
	
	/* Methods involving other Vector3:s */
	public void  add(Vector3 v) {x+=v.x;y+=v.y;z+=v.z;}
	public void  sub(Vector3 v) {x-=v.x;y-=v.y;z-=v.z;}
	public void  addScaled(Vector3 v, float f) {x+=f*v.x;y+=f*v.y;z+=f*v.z;}
	public void  subScaled(Vector3 v, float f) {x-=f*v.x;y-=f*v.y;z-=f*v.z;}
	
	/* */
	public void set(Vector2 v) {x=v.x; y=v.y; z=0.0f;}
	
	/* Scalar, component and cross product between two Vector3:s */
	public float	dot(Vector3 v)			{return x*v.x+y*v.y+z*v.z;}
	public void	cprodUpdate(Vector3 v)	{x*=v.x;y*=v.y; z*=v.z;}				// Component product...is stupid
	public Vector3 cprod(Vector3 v) {return new Vector3(x*v.x,y*v.y,z*v.z);}	// Component product...is stupid
	
	public Vector3 cross(Vector3 v) {
		return new Vector3(y*v.z-z*v.y ,z*v.x-x*v.z,x*v.y-y*v.x);
	}
	
	public void crossUpdate(Vector3 v) {
		float _x = y*v.z-z*v.y; 
		float _y = z*v.x-x*v.z;
		z = x*v.y-y*v.x;
		x =_x; y=_y;
	}
	
	/* Create an orthonormmal basis */
	public void makeOrthonormalBasis(Vector3 a, Vector3 b, Vector3 c) {
		a.normalize();
		c = a.cross(b);
		if(c.sqrdMagnitude() == 0.0) {return;}
		c.normalize();
		b = c.cross(a);
	}
}
