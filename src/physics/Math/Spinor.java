/* 
 * Spinor.java
 * In this case a "2D Quaternion equivalent"
 * Represents orientation in 2 dimensions
 * A spinor, z is a normalized complex number z=a+ib: {|z|=1}
 * where a = cos(theta) and b = i*sin(theta)
 *
 * NOTE: Uses float, not double
 * TODO: Nothing!
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.Math;

public class Spinor {
	public float a;
	public float b;
	
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
	public Spinor()						{a=1.0f;b=0.0f;}
	public Spinor(float _a, float _b)	{a=_a; b=_b;}
	public Spinor(Spinor s)				{a=s.a;b=s.b;}
	public Spinor(float rotation) {
		a = (float)Math.cos(rotation); b = (float)Math.sin(rotation);
	}
	
	/* toString */
	public String toString() {return a+" + "+b+"i: " + getOrientation();}
	
	/* --------------------------------------------------------------
	 * Getters and setters
	 * --------------------------------------------------------------
	 */
	public void set(Spinor s)			{a=s.a;b=s.b;}
	public void set(float _a, float _b)	{a=_a;b=_b; normalize();}
	
	/* -------------------------------------------------------------------------
	 * Multiply with scalar (float)
	 * -------------------------------------------------------------------------
	 */
	public void mult(float rotation) {
		Spinor delta = new Spinor(rotation);
		this.mult(delta);
	}
	
	public Spinor multNew(float rotation) {
		Spinor delta = new Spinor(rotation);
		Spinor spinor = new Spinor(this);
		spinor.mult(delta);
		return spinor;
	}
	
	public void multScaled(float rotation, float scale) {
		rotation *= scale;
		mult(rotation);
	}
	
	public Spinor multScaledNew(float rotation, float scale) {
		rotation *= scale;
		return multNew(rotation);
	}
	
	/* -------------------------------------------------------------------------
	 * multiply with other spinor
	 * -------------------------------------------------------------------------
	 */
	public void mult(Spinor s) {
		float _a = a*s.a - b*s.b;
		float _b = a*s.b + b*s.a;
		a = _a; b = _b;
	}
	
	public Spinor multNew(Spinor s) {
		Spinor spinor = new Spinor(this);
		spinor.mult(s);
		return spinor;
	}
	
	/* -------------------------------------------------------------------------
	 * Normalize 
	 * -------------------------------------------------------------------------
	 */
	public void normalize() {float n = 1.0f/(a*a+b*b); a *= n; b *= n;}
	
	/* -------------------------------------------------------------------------
	 * Give orientation in radians 
	 * Not very good...check for methods to get full -pi->pi range when
	 * converting using atan().
	 * -------------------------------------------------------------------------
	 */
	public float getOrientation() {
		
		if		(a>0.0f)				{return (float)Math.atan(b/a);}
		else if (a<0.0f && b >= 0.0f)	{return (float)(Math.atan(b/a) + Math.PI);}
		else if (a<0.0f && b <  0.0f)	{return (float)(Math.atan(b/a) - Math.PI);}
		else if (a==0.0f && b > 0.0f)	{return (float)(Math.PI)*0.5f;}
		else if (a==0.0f && b < 0.0f)	{return (float)(Math.PI)*-0.5f;}
		else							{a = 1.0f; return 0.0f;}
	}
	
	/* Give orientation in degrees, useful when rendering? */
	public float getOrientationDeg() {
		return (float) Math.toDegrees( getOrientation() );
	}
	
}