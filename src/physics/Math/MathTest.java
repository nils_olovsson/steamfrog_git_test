/*
 *
 *
 */
package physics.Math;

public class MathTest {
	
	
	
	
	/* -------------------------------------------------------------------------
	 * Math testing
	 * -------------------------------------------------------------------------
	 */
	public static void mathTest() {
		System.out.println("Lets test some matrices!");
		
		Vector3 v1, v2, v3, v4;
		Vector2 p1, p2, p3, p4;
		Matrix3 m1, m2, m3, m4;
		Spinor	s1, s2, s3, s4;
		
		p1 = new Vector2();
		
		// Create some vectors
		v1 = new Vector3(1.0f, 0.0f, 3.4f);
		v2 = new Vector3(0.0f, 2.1f,-0.4f);
		v3 = new Vector3(0.4f, 2.8f, 6.1f);
		v4 = new Vector3(-7.0f,6.5f, 0.4f);

		p1 = new Vector2(1.0f, 0.0f);
		p2 = new Vector2(0.0f, 2.1f);
		p3 = new Vector2(0.4f, 2.8f);
		p4 = new Vector2(-7.0f,6.5f);
		
		// Create some spinors
		s1 = new Spinor( 0.0f); s1.normalize();
		s2 = new Spinor( 0.7f,-1.0f); s2.normalize();
		s3 = new Spinor(	 (float)Math.PI); s3.normalize();
		s4 = new Spinor(-0.5f*(float)Math.PI); s4.normalize();
		
		s1.mult(s2);
		s1.multScaled(0.1f, 2.0f);
		//s1.mult(s4);
		//s1.mult(s2);
		
		// Create the matrices
		m1 = new Matrix3(v1, v2, v3);
		m2 = new Matrix3(v4, v2, v3);
		//m3 = new Matrix3();
		m4 = new Matrix3();
		m4.setInverse(m1);
		
		// Do some matrix-matrix operations
		m3 = m1.cross(m2);
		System.out.println("Matrix1:\n"			+ m1);
		System.out.println("Matrix1.tran():\n"	+ m1.transposeNew());
		System.out.println("Matrix2:\n"			+ m2);
		System.out.println("Matrix1*Matrix2:\n" + m3);
		System.out.println("Matrix1.inv(): \n"	+ m1.inverseNew());
		System.out.println("Matrix1.det(): \n"	+ m1.determinant());
		
		// Do some matrix-vector3 operations
		v3 = m1.cross(v1);
		v4 = m2.cross(v2);
		System.out.println("\n\nMatrix-Vector3 operations!");
		System.out.println("Matrix1*Vector1 = " + v3);
		System.out.println("Matrix2*Vector2 = " + v4);
		
		
		// Do some matrix-vector2 operations
		p3 = m1.cross(p1);
		p4 = m2.cross(p2);
		System.out.println("\n\nMatrix-Vector2 operations!");
		System.out.println("Matrix1*Vector1 = " + p3);
		System.out.println("Matrix2*Vector2 = " + p4);
		
		// Do some spinor tests
		System.out.println("\n\nSpinors!");
		System.out.println("Spinor1 = " + s1);
		System.out.println("Spinor2 = " + s2);
		System.out.println("Spinor3 = " + s3);
		System.out.println("Spinor4 = " + s4);
		
		System.out.println("\nEnd of test.");
	} // End of mathTest()
}
