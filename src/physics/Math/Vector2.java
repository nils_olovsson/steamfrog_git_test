/*
 * Vector2.java
 * Basic row vector with two values and some basic operations.
 * 
 * NOTE: Uses float, not double
 * TODO: Nothing!
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.Math;

public class Vector2 {
	
	public float x,y;
    
	/* Constructors */
	public Vector2()						{x = 0.0f;		y = 0.0f;}
	public Vector2(float _x,  float _y)		{x = _x;		y = _y;}
	public Vector2(double _x, double _y)	{x = (float)_x; y = (float)_y;}
	public Vector2(Vector2 v)				{x = v.x;		y = v.y;}
	public Vector2(Vector3 v)				{x = v.x;		y = v.y;}
	
	/* toString() */
	public String toString() {return "["+x+", "+y+"]";}
	
	public void clear()	{x=0.0f;y=0.0f;}
	public void zero()		{x=0.0f; y=0.0f;}
	
	/* Invert, normalization and magnitude methods */
	public void invert() {x=-x;y=-y;}
	public float magnitude()	 {return (float)Math.sqrt(x*x+y*y);}
	public float sqrdMagnitude() {return x*x+y*y;}
	public void normalize() {float im = 1/magnitude(); if(im>0){x*=im; y*=im;}};
	// Is a fast bithack norm possible in java?
	
	/* Methods involving scalars */
	public void add(float f)	{x+=f;y+=f;}
	public void sub(float f)	{x-=f;y-=f;}
	public void mult(float f)	{x*=f;y*=f;}
	public void div(float f)	{x/=f;y/=f;}
	
	public Vector2 addNew(float f)	{return new Vector2(x+f,y+f);}
	public Vector2 subNew(float f)	{return new Vector2(x-f,y-f);}
	public Vector2 multNew(float f){return new Vector2(x*f,y*f);}
	public Vector2 divNew(float f)	{return new Vector2(x/=f,y/=f);}
	
	public void add(double f)	{x+=(float)f;y+=(float)f;}
	public void sub(double f)	{x-=(float)f;y-=(float)f;}
	public void mult(double f)	{x*=(float)f;y*=(float)f;}
	public void div(double f)	{x/=(float)f;y/=(float)f;}
	
	public Vector2 addNew(double d)	{return new Vector2(x+(float)d,y+(float)d);}
	public Vector2 subNew(double d)	{return new Vector2(x-(float)d,y-(float)d);}
	public Vector2 multNew(double d){return new Vector2(x*(float)d,y*(float)d);}
	public Vector2 divNew(double d)	{return new Vector2(x/=(float)d,y/=(float)d);}
	
	public void set (float _x, float _y)	{x=_x;y=_y;}
	public void setX(float _x)				{x=_x;}
	public void setY(float _y)				{y=_y;}
	
	/* Methods involving other Vector2:s */
	public void  set(Vector2 v) {x=v.x; y=v.y;}
	public void  add(Vector2 v) {x+=v.x;y+=v.y;}
	public void  sub(Vector2 v) {x-=v.x;y-=v.y;}
	public void  addScaled(Vector2 v, float f) {x+=f*v.x;y+=f*v.y;}
	public void  subScaled(Vector2 v, float f) {x-=f*v.x;y-=f*v.y;}
	
	public Vector2 addNew(Vector2 v)				{return new Vector2(x+v.x,y+v.y);}
	public Vector2 subNew(Vector2 v)				{return new Vector2(x-v.x,y-v.y);}
	public Vector2 addScaledNew(Vector2 v, float f) {return new Vector2(x+f*v.x,y+f*v.y);}
	public Vector2 subScaledNew(Vector2 v, float f) {return new Vector2(x-f*v.x,y-f*v.y);}
	
	/* Scalar, component and cross product between two Vector2:s */
	/* Cross product doesn't exist in 2D space!!! */
	public float	dot(Vector2 v)			{return x*v.x+y*v.y;}
	public void	cprodUpdate(Vector2 v)	{x*=v.x;y*=v.y;}			 // Component product...is stupid
	public Vector2 cprod(Vector2 v) {return new Vector2(x*v.x,y*=v.y);} // Component product...is stupid
	public Vector2 perpendicular()	 {return new Vector2(-y,x);}
	
	/* Cross multiplication that just returns the z-component */
	public float cross(Vector2 v) {return x*v.y - y*v.x;}
}
