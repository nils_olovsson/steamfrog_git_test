/*
 * ContactGenerator.java
 * Generates a contact that acts upon a RigidBody without the need for that
 * object to be in contact with another RigidBody.
 * 
 * 
 * NOTE:
 * TODO: Werks werd
 * 
 * Author: Nils Olofsson
 * Last Revised:
 */
package physics.RigidPhysics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import physics.Math.Vector2;

public interface ContactGenerator {
	
	public void addContact(ArrayList contacts);
	public void render(Graphics2D _g2d);
	public boolean containsInside(RigidBody body);
	
	/* -------------------------------------------------------------------------
	 * Connects two bodies together or one body to a point in space
	 * -------------------------------------------------------------------------
	 */
	public static abstract class Link implements ContactGenerator {
		RigidBody rb					= null;
		public RigidBody rb2			= null;
		float friction					= 0.0f;
		float restitution				= 0.0f;
		public Vector2 connectionPoint1 = new Vector2();
		public Vector2 connectionPoint2 = new Vector2();
		
		public void setConnection1(float x, float y){connectionPoint1.set(x,y);}
		public void setConnection2(float x, float y){connectionPoint2.set(x,y);}
		public void setConnection1(Vector2 v)		{connectionPoint1.set(v);}
		public void setConnection2(Vector2 v)		{connectionPoint2.set(v);}
		
		public float currentLength() {
			
			// Transform points to world space
			Vector2 pos1 = rb.transformMatrix.trans(connectionPoint1);
			Vector2 pos2;
			if(rb2 != null) {
				pos2 = rb2.transformMatrix.trans(connectionPoint2);
			} else {
				pos2 = connectionPoint2;
			}
			
			// Calculate lentgh
			pos1.sub(pos2);
			
			return pos1.magnitude();
		}
		
		/* ----------------------------------------------------------
		 * 
		 * ----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d) {
			
			// Transform points to world space
			Vector2 pos1 = rb.transformMatrix.trans(connectionPoint1);
			Vector2 pos2;
			if(rb2 != null) {
				pos2 = rb2.transformMatrix.trans(connectionPoint2);
			} else {
				pos2 = connectionPoint2;
			}

			int x1, x2, y1, y2;
			x1 = (int)pos1.x; y1 = (int)pos1.y;
			x2 = (int)pos2.x; y2 = (int)pos2.y;
			_g2d.setColor(Color.BLUE);
			_g2d.drawLine(x1, y1, x2, y2);
		}
		
		/* ----------------------------------------------------------
		 * 
		 * ----------------------------------------------------------
		 */
		public boolean containsInside(RigidBody body) {
			return rb == body || rb2 == body;
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Connects two RigidBodies with a cable that prevents bodies
	 * from moving too far away from each other.
	 * -------------------------------------------------------------------------
	 */
	public static class Cable extends Link {
		
		float maxLength;
		
		/* ----------------------------------------------------------
		 * Constructor
		 * ----------------------------------------------------------
		 */
		public Cable() {
			rb2 = null;
			maxLength = 0.0f;
		}
		public Cable(RigidBody _rb1, RigidBody _rb2)	{
			rb = _rb1; rb2 = _rb2;
			maxLength = 0.0f;
		}
		public Cable(RigidBody _rb1, RigidBody _rb2, float mL)	{
			rb = _rb1; rb2 = _rb2;
			maxLength = mL;
		}
		
		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public void setMaxLength(float f){maxLength = f;}
		public float getMaxLength(){return maxLength;}
		
		/* ----------------------------------------------------------
		 * Contact check
		 * ----------------------------------------------------------
		 */
		public void addContact(ArrayList contacts) {
			float length = currentLength();
			
			// Check if contact
			if( length<maxLength ) {return;}
			
			Vector2 pos1 = rb.transformMatrix.trans(connectionPoint1);
			Vector2 pos2;
			if(rb2 != null) {
				pos2 = rb2.transformMatrix.trans(connectionPoint2);
			} else {
				pos2 = connectionPoint2;
			}
			
			NarrowCollision.Contact contact = new NarrowCollision.Contact();
			
			contact.contactNormal = pos1.subNew(pos2);
			contact.contactNormal.normalize();
			contact.contactPoint = pos2.addNew(contact.contactNormal.multNew(length));
			contact.penetration	 = maxLength - length;
			contact.rb1 = rb;
			contact.rb2 = rb2;
			contacts.add(contact);
		}
	
	} // End of inner class Cable
	
	/* ---------------------------------------------------------------
	 * 
	 * ---------------------------------------------------------------
	 */
	//public class AnchoredCable extends Link {
		
		/* ------------------------------------------------
		 * Constructor
		 * ------------------------------------------------
		 */
		//public AnchoredCable() {}
	//} // End of inner class AnchoredCable
	
}
