/*
 * ForceGenerator.java
 * Generates a force that acts upon a RigidBody. The force may be applied so
 * that it generates only a linear change (applied to position 0,0 aka center of
 * mass of the rigid body) or at a position so that it also generates a torque
 * that will try to rotate the body.
 * Acutall forces are applied through a ForceRegistry.
 * 
 * This file contains two inner public classes:
 *		Gravity - Gives a force along the y-axis
 *		Spring - Gives a force on ONE RigidBody like a spring
 *				(push if near, pull if far away)
 * 
 * The forces also has a rendering function so they can be visualised.
 * 
 * NOTE:
 * TODO: Implement more forces! Bungee rope, spring that acts on both bodies,
 *		 and anchored spring attached at fixed point in space.
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.awt.Color;
import java.awt.Graphics2D;
import physics.Math.Vector2;

public interface ForceGenerator {
	
	/* -------------------------------------------------------------------------
	 * "Abstract" methods
	 * -------------------------------------------------------------------------
	 */
	public void updateForce(RigidBody body, float dt);
	public void render(Graphics2D _g2d, RigidBody body);
	public boolean containsInside(RigidBody body);
	
	/* -------------------------------------------------------------------------
	 * Inner class Gravity that implements the ForceGenerator interface
	 * -------------------------------------------------------------------------
	 */
	public class Gravity implements ForceGenerator {
	
		private Vector2 gravity;
	
		/* ----------------------------------------------------------
		 * Constructor
		 * -----------------------------------------------------------
		 */
		public Gravity()					{gravity = new Vector2(0.0f, 10.0f);}
		public Gravity(float g)				{gravity = new Vector2(0.0f, g);}
		public Gravity(float x, float y)	{gravity = new Vector2(x,y);}

		/* ----------------------------------------------------------
		 * 
		 * -----------------------------------------------------------
		 */
		public void updateForce(RigidBody body, float dt) {
			// Check that we do not have infinite mass
			if( !body.hasFiniteMass() ) {return;}

			// Apply the mass-scaled force to the body
			body.addForce( gravity.multNew(body.getMass()) );
		}

		/* ----------------------------------------------------------
		 * Renders a line at CoM of body in the direction of gravity
		 * -----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d, RigidBody body) {
			Vector2 pos		= body.getPosition();
			int x1, y1, x2, y2;
			x1 = (int)pos.x;			y1 = (int)pos.y;
			x2 = x1 + (int)gravity.x;	y2 = y1 + (int)gravity.y;
			_g2d.setColor(Color.RED);
			_g2d.drawLine(x1, y1, x2, y2);
		}
		
		/* ----------------------------------------------------------
		 * Contains other particle?
		 * ----------------------------------------------------------
		 */
		public boolean containsInside(RigidBody body) {return false;}
		
	} // End of inner class Gravity
	
	/* -------------------------------------------------------------------------
	 * Inner class Spring that implements the ForceGenerator interface
	 * -------------------------------------------------------------------------
	 */
	public class Spring implements ForceGenerator{

		Vector2 connectionPoint;		// Connection in object space to first body
		Vector2 otherConnectionPoint;	// Connection in object space to second body
		RigidBody other;				// The body at the other end of the spring
		float springConstant;			// 
		float restLength;				// 

		/* -------------------------------------------------------------------------
		 * Constructors
		 * -------------------------------------------------------------------------
		 */
		public Spring () {
			connectionPoint			= new Vector2();	// Connect to CoM
			otherConnectionPoint	= new Vector2();	// Connect to CoM
			other					= null;
			springConstant			= 1.0f;
			restLength				= 40.0f;
		}

		public Spring (RigidBody _other) {
			connectionPoint			= new Vector2();	// Connect to CoM
			otherConnectionPoint	= new Vector2();	// Connect to CoM
			other					= _other;
			springConstant			= 1.0f;
			restLength				= 40.0f;
		}

		public Spring (Spring s) {
			connectionPoint			= new Vector2(s.connectionPoint);
			otherConnectionPoint	= new Vector2(s.otherConnectionPoint);
			other					= s.other;
			springConstant			= s.springConstant;
			restLength				= s.restLength;
		}

		/* -------------------------------------------------------------------------
		 * Getters and setters
		 * -------------------------------------------------------------------------
		 */
		public void setConnectionPoint1(Vector2 v)			{connectionPoint.set(v);}
		public void setConnectionPoint1(float x, float y)	{connectionPoint.set(x, y);}
		public Vector2 getConnectionPoint1()				{return new Vector2(connectionPoint);}
		public void setConnectionPoint2(Vector2 v)			{otherConnectionPoint.set(v);}
		public void setConnectionPoint2(float x, float y)	{otherConnectionPoint.set(x, y);}
		public Vector2 getConnectionPoint2()				{return new Vector2(otherConnectionPoint);}
		public void setOtherBody(RigidBody o)				{other = o;}
		public RigidBody getOtherBody(RigidBody o)			{return other;}
		public void setSpringConstant(float k)				{springConstant = k;}
		public float getSpringConstant()					{return springConstant;}
		public void setRestLength(float l)					{restLength = l;}
		public float getRestLength()						{return restLength;}

		/* -------------------------------------------------------------------------
		 * 
		 * -------------------------------------------------------------------------
		 */
		public void updateForce(RigidBody body, float dt) {

			if( other == null ) {return;}

			// Calculate the two ends in world space
			Vector2 lws = body.getPointInWorldSpace(connectionPoint);
			Vector2 ows = other.getPointInWorldSpace(otherConnectionPoint);

			// Calculate the vector of the spring
			Vector2 force = new Vector2(lws); force.sub(ows);

			// Calculate the magnitude of the force
			float magnitude = force.magnitude();
			magnitude = magnitude-restLength;
			magnitude *= springConstant;

			// Calculate the final force and apply it
			force.normalize();
			force.mult( -magnitude );

			body.addForceAtPoint(force, lws);
		}

		/* -------------------------------------------------------------------------
		 * Render this spring as a line between its connection points in world space
		 * -------------------------------------------------------------------------
		 */
		public void render(Graphics2D _g2d, RigidBody body) {
			// Calculate the two ends in world space
			Vector2 lws = body.getPointInWorldSpace(connectionPoint);
			Vector2 ows = other.getPointInWorldSpace(otherConnectionPoint);

			int x1, x2, y1, y2;
			x1 = (int)lws.x; y1 = (int)lws.y;
			x2 = (int)ows.x; y2 = (int)ows.y;
			_g2d.setColor(Color.YELLOW);
			_g2d.drawLine(x1, y1, x2, y2);
		}
		
		/* ----------------------------------------------------------
		 * Contains other particle?
		 * ----------------------------------------------------------
		 */
		public boolean containsInside(RigidBody body) {return other==body;}
		
	} // End of inner class Spring
	
	/* -------------------------------------------------------------------------
	 * Inner class BungeeRope that implements the ForceGenerator interface
	 * -------------------------------------------------------------------------
	 */
	public class BungeeRope implements ForceGenerator{

		Vector2 connectionPoint;		// Connection in object space to body
		Vector2 otherConnectionPoint;	// Connection point relative to world
		float springConstant;			// 
		float restLength;				// 

		/* ----------------------------------------------------------
		 * Constructors
		 * ----------------------------------------------------------
		 */
		public BungeeRope () {
			connectionPoint			= new Vector2();	// Connect to CoM
			otherConnectionPoint	= new Vector2();
			springConstant			= 2.0f;
			restLength				= 40.0f;
		}

		public BungeeRope (BungeeRope b) {
			connectionPoint			= new Vector2(b.connectionPoint);
			otherConnectionPoint	= new Vector2(b.otherConnectionPoint);
			springConstant			= b.springConstant;
			restLength				= b.restLength;
		}

		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public void setConnectionPoint1(Vector2 v)			{connectionPoint.set(v);}
		public void setConnectionPoint1(float x, float y)	{connectionPoint.set(x, y);}
		public Vector2 getConnectionPoint1()				{return new Vector2(connectionPoint);}
		public void setConnectionPoint2(Vector2 v)			{otherConnectionPoint.set(v);}
		public void setConnectionPoint2(float x, float y)	{otherConnectionPoint.set(x, y);}
		public Vector2 getConnectionPoint2()				{return new Vector2(otherConnectionPoint);}
		public void setSpringConstant(float k)				{springConstant = k;}
		public float getSpringConstant()					{return springConstant;}
		public void setRestLength(float l)					{restLength = l;}
		public float getRestLength()						{return restLength;}

		/* ----------------------------------------------------------
		 * 
		 * ----------------------------------------------------------
		 */
		public void updateForce(RigidBody body, float dt) {

			// Calculate the two ends in world space
			Vector2 lws = body.getPointInWorldSpace(connectionPoint);
			Vector2 ows = otherConnectionPoint;

			// Calculate the vector of the spring
			Vector2 force = new Vector2(lws); force.sub(ows);

			// Calculate the magnitude of the force
			float magnitude = force.magnitude();
			if(magnitude<=restLength) {return;}	// No stretching
			magnitude = magnitude-restLength;
			magnitude *= springConstant;

			// Calculate the final force and apply it
			force.normalize();
			force.mult( -magnitude );

			body.addForceAtPoint(force, lws);
		}

		/* -------------------------------------------------------------------------
		 * Render this spring as a line between its connection points in world space
		 * -------------------------------------------------------------------------
		 */
		public void render(Graphics2D _g2d, RigidBody body) {
			// Calculate the two ends in world space
			Vector2 lws = body.getPointInWorldSpace(connectionPoint);
			Vector2 ows = otherConnectionPoint;

			int x1, x2, y1, y2;
			x1 = (int)lws.x; y1 = (int)lws.y;
			x2 = (int)ows.x; y2 = (int)ows.y;
			_g2d.setColor(Color.YELLOW);
			_g2d.drawLine(x1, y1, x2, y2);
		}
		
		public boolean containsInside(RigidBody body){return false;}
	} // End of inner class Spring
	
} // End of interface ForceGenerator
