/*
 *
 * 
 * 
 * 
 * 
 * 
 */
package physics.RigidPhysics;

import java.awt.Graphics2D;
import java.util.ArrayList;

import physics.RigidPhysics.ForceGenerator.*;
import physics.RigidPhysics.NarrowCollision.*;

public class World {
	ArrayList bodies;
	ForceRegistry registry;
	ArrayList contactGenerators;
	CoarseCollision coarseCollisionDetector;
	NarrowCollision narrowCollisionDetector;
	ContactResolver resolver;

	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
	public World() {
		bodies = new ArrayList<RigidBody>();
		registry = new ForceRegistry();
		contactGenerators = new ArrayList<ContactGenerator>();

		// Collision checking systems
		coarseCollisionDetector = new CoarseCollision();
		narrowCollisionDetector = new NarrowCollision();
		resolver = new ContactResolver();
	}

	/* -------------------------------------------------------------------------
	 * Some getters and setters
	 * -------------------------------------------------------------------------
	 */
	public int getNrOfBodies() {return bodies.size();}
	
	public ArrayList getContacts() {
		return narrowCollisionDetector.contacts;
	}
	
	public void removeContact(Contact contact) {
		narrowCollisionDetector.contacts.remove(contact);
	}

	public void setContacts(ArrayList contacts) {
		narrowCollisionDetector.contacts = contacts;
	}
	
	/* -------------------------------------------------------------------------
	 * Add
	 * -------------------------------------------------------------------------
	 */
	public void addBody(RigidBody body)	{bodies.add(body);}
	
	public void addForceToAll(ForceGenerator force) {
		int length = bodies.size();
		RigidBody currentBody;

		for(int i=0;i<length;i++) {
			currentBody = (RigidBody)bodies.get(i);
			if(!currentBody.isStatic()){registry.add(currentBody, force);}
		}
	}
	
	public void addForce(ForceGenerator force, int index) {
		try {
			RigidBody body		= (RigidBody)bodies.get(index);
			if(body!=null)
				registry.add(body, force);
		}
		catch(Exception e){}
	}
	
	public void addForce(ForceGenerator force, RigidBody body) {
		if( !bodies.contains(body) ) {bodies.add(body);}
		registry.add(body, force);
	}
	
	public void addGravityToAll() {
		ForceGenerator gravity = new Gravity(200.0f);
		addForceToAll(gravity);
	}
	
	public void addGravityToAll(float g) {
		ForceGenerator gravity = new Gravity(g);
		addForceToAll(gravity);
	}
	
	public void addContactGenerator(ContactGenerator cg) {
		contactGenerators.add(cg);
	}
	
	/* -------------------------------------------------------------------------
	 * Remove various things from other things
	 * -------------------------------------------------------------------------
	 */
	public void remove(RigidBody body)	{
		bodies.remove(body);
		registry.remove(body);
		removeContactGenerator(body);
	}
	
	public void removeContactGenerator(RigidBody body) {
		int i, length = contactGenerators.size();
		ContactGenerator current;
		ArrayList removeRegister = new ArrayList<ContactGenerator>();
		
		// Find generators to remove
		for(i=0;i<length;i++) {
			current = (ContactGenerator)contactGenerators.get(i);
			if(current.containsInside(body)) {removeRegister.add(current);}
		}
		
		// Remove the found generators
		for(i=0;i<length;i++) {
			current = (ContactGenerator)removeRegister.get(i);
			contactGenerators.remove(current);
		}
	}

	/* -------------------------------------------------------------------------
	 * Clears force accumulators for all bodies and empties
	 * contact register to prepare for new frame.
	 * -------------------------------------------------------------------------
	 */
	public void startFrame() {
		int length = bodies.size();
		RigidBody	body;

		for(int i=0;i<length;i++) {
			body = (RigidBody)bodies.get(i);
			if(body!=null) {
				body.clearAccumulators();
				body.calculateDerivedData();
			}
		}

		// Clear the collision data
		coarseCollisionDetector.clear();
		narrowCollisionDetector.clear();
	}

	/* -------------------------------------------------------------------------
	 * Integrate all bodies
	 * -------------------------------------------------------------------------
	 */
	public void integrate(float dt) {
		int length = bodies.size();
		RigidBody	body;

		for(int i=0;i<length;i++) {
			body = (RigidBody)bodies.get(i);
				body.integrate(dt);
		}
	}

	/* -------------------------------------------------------------------------
	 * Run physics
	 * -------------------------------------------------------------------------
	 */
	public void runPhysics(float dt) {
		// Clear all accumulators etc
		startFrame();

		// First apply the force generators
		registry.updateForces(dt);

		// Then integrate the objects
		integrate(dt);
		
		// Generate and solve contacts
		coarseCollisionDetector.buildContactList(bodies);
		narrowCollisionDetector.buildContactList(
			coarseCollisionDetector.getContactList()
			);
		narrowCollisionDetector.calculateAllInternals(dt);
		
		generateContacts();
		resolver.resolveContacts(narrowCollisionDetector.contacts,dt);
	}
	
	/* -------------------------------------------------------------------------
	 * Run physics in parts, so that for example contact data can be extracted
	 * to game in middle of timestep
	 *
	 * Steps in order of supposed execution:
	 * 1. runForces()			- Start new frame, apply forces, integrate
	 * 2. runGenerateContacts() - Find true contacts and build list
	 * 3. runSolveContacts()	- Solve true contacts
	 * -------------------------------------------------------------------------
	 */
	public void runForces(float dt) {
		// Clear all accumulators etc
		startFrame();

		// First apply the force generators
		registry.updateForces(dt);

		// Then integrate the objects
		integrate(dt);
	}
	
	public void runGenerateContacts(float dt) {
		// Generate and solve contacts
		coarseCollisionDetector.buildContactList(bodies);
		narrowCollisionDetector.buildContactList(
			coarseCollisionDetector.getContactList()
			);
		
		generateContacts();
		//narrowCollisionDetector.calculateAllInternals(dt);
	}
	
	public void runSolveContacts(float dt) {
		resolver.resolveContacts(narrowCollisionDetector.contacts,dt);
	}

	/* -------------------------------------------------------------------------
	 * Iterate through list of contact generators
	 * -------------------------------------------------------------------------
	 */
	public void generateContacts() {
		int length = contactGenerators.size();
		ContactGenerator current;

		for(int i=0;i<length;i++) {
			current = (ContactGenerator)contactGenerators.get(i);
			current.addContact(narrowCollisionDetector.contacts);
		}
	}

	/* -------------------------------------------------------------------------
	 * Render force generators such as springs
	 * -------------------------------------------------------------------------
	 */
	public void renderForceGenerators(Graphics2D _g2d) {
		registry.render(_g2d);
	}

	/* -------------------------------------------------------------------------
	 * Render contact generators such as cables
	 * -------------------------------------------------------------------------
	 */
	public void renderContactGenerators(Graphics2D _g2d) {
		int length = contactGenerators.size();
		ContactGenerator current;

		for(int i=0;i<length;i++) {
			current = (ContactGenerator)contactGenerators.get(i);
			current.render(_g2d);
		}
	}

	/* -------------------------------------------------------------------------
	 * Render the bounding geometry associated with this RB
	 * -------------------------------------------------------------------------
	 */
	public void renderBoundingGeom(Graphics2D _g2d) {
		int length = bodies.size();
		RigidBody body;

		for(int i=0;i<length;i++) {
			// Render the bounding geometry
			body = (RigidBody)bodies.get(i);
			body.renderBoundingGeom(_g2d);
		}
	}

	/* -------------------------------------------------------------------------
	 * Render the bounding geometry associated with this RB
	 * -------------------------------------------------------------------------
	 */
	public void renderContactGeom(Graphics2D _g2d) {
		int length = bodies.size();
		RigidBody body;

		for(int i=0;i<length;i++) {
			// Render the bounding geometry
			body = (RigidBody)bodies.get(i);
			body.renderContactGeom(_g2d);
		}
	}

	/* -------------------------------------------------------------------------
	 * Render contact information such as point and normal
	 * ----------------------------------------------------------------------
	 */
	public void renderContacts(Graphics2D _g2d) {
		narrowCollisionDetector.render(_g2d);
	}

	/* -------------------------------------------------------------------------
	 * Draw body information
	 * ----------------------------------------------------------------------
	 */
	public void drawInfo(Graphics2D _g2d, int x, int y) {
		//Draw number of bodies
		//Draw number of ForceGenerators
		//Maybe something more
	}

} // End of class World