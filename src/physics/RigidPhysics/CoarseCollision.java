/*
 * CoarseCollision.java
 * Contains everything for broad collision detection between bounding rectangles
 * and circles. Builds a list of these potential collisions that may be sent to
 * a check for narrow collision.
 * This file contains three inner public classes:
 *		PotentialContact - Basically just a container for two RigidBodies.
 *		BoundingGeometry - An abstract class for the other Bounding-classes
 *		BoundingCircle	 - A circle that should completely encapsulate object
 *		BoundingRectangle- A rectangle that should completely encapsulate object
 * 
 * NOTE: The code for Separate Axis Test (SAT) between two rectangles here is
 *		 different and more stupid compared to the one in the narrow check.
 *		 Well, thats because I wrote it from scratch.
 * 
 * NOTE: If ContactGeometry is not fully encapsulated by BoundingGeometry
 *		 abnormal behaviour will become norm.
 *
 * Bodies in the list of potentialContacts may be null. This likely means they
 * are static parts of the environment like walls or floors that should not be
 * able to move. This is handled in the collision resolver.
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import physics.Math.Vector2;
import physics.Math.Matrix3;

public class CoarseCollision {
	
	public ArrayList potentialCollisions;
	public int maxNrOfCollisions;
	public int currentNrOfCollisions;
	
	/* Enumerated geometry type for coarse collision */
	public enum Type {CIRCLE, RECTANGLE}
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
	public CoarseCollision() {
		potentialCollisions = new ArrayList<PotentialContact>();
		currentNrOfCollisions = 0;
	}
	
	/* -------------------------------------------------------------------------
	 * Inner class PotentialContact, contains two bodies that might
	 * be in contact with each other. This data is sent to the
	 * system for narrow collision detection
	 * -------------------------------------------------------------------------
	 */
	public class PotentialContact {
		RigidBody rb1;
		RigidBody rb2;
		
		/* ----------------------------------------------------------
		 * Constructor
		 * ----------------------------------------------------------
		 */
		public PotentialContact() { rb1=null; rb2 = null;}
		public PotentialContact(RigidBody _rb1, RigidBody _rb2) {
			rb1 = _rb1; rb2 = _rb2;
		}
		
		/* ----------------------------------------------------------
		 * Check if two potential contacts contains the same objects
		 * ----------------------------------------------------------
		 */
		public boolean equals(PotentialContact other) {
			return	rb1==other.rb1 && rb2==other.rb2 ||
					rb1==other.rb2 && rb2==other.rb1;
		}
		
		/* ----------------------------------------------------------
		 * Clear by setting both references to null
		 * ----------------------------------------------------------
		 */
		public void clear() {rb1 = null; rb2 = null;}
	}
	
	/* -------------------------------------------------------------------------
	 * Inner classes for bounding geometry, for broad phase collision
	 * an object can have a bounding Circle or a Rectangle.
	 * They can also collide with lines that might be the bounds of 
	 * a map.
	 * -------------------------------------------------------------------------
	 */
	public static abstract class BoundingGeometry {
	
		public Vector2 center;	// Contains the center position of the bounding
								// volume in object coordinates. (The coordinates of
								// the rigid body...)
		
		// Used for rendering with a color switch on contact
		protected boolean isInContact;
		protected long contactTime;	// Time of collision, used for rendering
		
		public Color color;		// Color used when rendering the bounding geom
		public Color contactColor;	// Color used when collision occurs

		/* Absract constructor, is empty */
		public BoundingGeometry() {
			color = Color.GREEN; contactColor = Color.RED;
		}

		/* ----------------------------------------------------------
		* Some abstract functions
		* -----------------------------------------------------------
		*/
		public abstract boolean	collision(	BoundingGeometry other,
											Matrix3 thisTrans,
											Matrix3 otherTrans);
		public abstract Type	getType();
		public abstract void render(Graphics2D _g2d, Matrix3 thisTrans, float theta);

		
		/* ----------------------------------------------------------
		 * Method for colliding a rectangle and a circle, used by
		 * both the BoundingCircle and the BoundingRectangle so
		 * defined here.
		 * Work in rectangles coordinate system
		 * ----------------------------------------------------------
		 */
		public boolean collision(	BoundingCircle circle,
									BoundingRectangle rect,
									Matrix3 circleTrans,
									Matrix3 rectTrans) {
			
			Vector2 circleCenter = circleTrans.trans(circle.getCenter());
					circleCenter = rectTrans.invTrans(circleCenter);
			Vector2 _halfSize	 = rect.getHalfSize();
			Vector2 _center		 = rect.getCenter();
			
			// Create corners of this rectangle in rectangles space
			Vector2 cornerA1 = new Vector2(_center.x+_halfSize.x,_center.y-_halfSize.y);
			Vector2 cornerA2 = new Vector2(_center.x+_halfSize.x,_center.y+_halfSize.y);
			Vector2 cornerA3 = new Vector2(_center.x-_halfSize.x,_center.y+_halfSize.y);
			Vector2 cornerA4 = new Vector2(_center.x-_halfSize.x,_center.y-_halfSize.y);
			
			// Check if circle center lies inside rectangle
			if(	circleCenter.x>=cornerA3.x && circleCenter.x<=cornerA2.x &&
				circleCenter.y>=cornerA1.y && circleCenter.y<=cornerA2.y) {
				return true;
			}
			
			// Check if any of the edges intersect the circle by comparing
			if(	circleCenter.y < 0.0f && circleCenter.y > -_halfSize.y-circle.radius)	{
				if(	circleCenter.x < 0.0f && circleCenter.x > -_halfSize.x-circle.radius ||
					circleCenter.x > 0.0f && circleCenter.x < _halfSize.x+circle.radius)
				{return true;}
			}
			
			if(	circleCenter.y > 0.0f && circleCenter.y < _halfSize.y+circle.radius)	{
				if(	circleCenter.x < 0.0f && circleCenter.x > -_halfSize.x-circle.radius ||
					circleCenter.x > 0.0f && circleCenter.x < _halfSize.x+circle.radius)
				{return true;}
			}
			
			return false;
		}
		
		/* ----------------------------------------------------------
		* Getters and setters
		* -----------------------------------------------------------
		*/
		public Vector2	getCenter()					{return center;}
		public void		setCenter(Vector2 v)		{center.set(v);}
		public void		setCenter(float x, float y) {center.set(x,y);}
	} // End of inner Abstract class BoundingGeometry
	
	/* -------------------------------------------------------------------------
	 * Inner class BoundingCircle
	 * -------------------------------------------------------------------------
	 */
	public static class BoundingCircle extends BoundingGeometry {
		public float radius;
		
		/* ----------------------------------------------------------
		 * Constructor
		 * ----------------------------------------------------------
		 */
		public BoundingCircle() {center = new Vector2(); radius = 1.0f;}
		public BoundingCircle(float r) {this.center = new Vector2(); this.radius = r;}
		public BoundingCircle(BoundingCircle bc) {
			center = new Vector2(bc.center);
			radius = bc.radius;
		}
		
		public BoundingCircle(Vector2 c, float r) {
			center = new Vector2(center);
			radius = r;
		}
		
		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public float getRadius()		{return radius;}
		public void setRadius(float r)	{radius = r;}
		public void setRadius(Vector2 v){if(v.x>v.y){radius=v.x;}else{radius=v.y;}}
		public Type	getType()			{return Type.CIRCLE;}
		
		/* ----------------------------------------------------------
		 * Collision detection
		 * ----------------------------------------------------------
		 */
		public boolean collision(	BoundingGeometry other,
									Matrix3 thisTrans,
									Matrix3 otherTrans) {
			
			/* Circle with circle collision */
			if( other.getType() == Type.CIRCLE ) {
				// Cast other object to circle object
				BoundingCircle otherCircle = (BoundingCircle)other;
				return collision(otherCircle,thisTrans,otherTrans);
				
			/* Circle with rectangle collision */
			} else if(  other.getType() == Type.RECTANGLE ) {
				// Cast other object to rectangle object
				BoundingRectangle rectangle = (BoundingRectangle)other;
				return collision(this, rectangle, thisTrans, otherTrans);
			}
			
			// Something unexpected has happened
			return false;
		}
		
		/* ----------------------------------------------------------
		 * Collide this circle with another circle
		 * ----------------------------------------------------------
		 */
		public boolean collision(	BoundingCircle other,
									Matrix3 thisTrans,
									Matrix3 otherTrans) {
				// Transform positions to world coordinates
				Vector2 thisCenter	= thisTrans.trans(center);
				Vector2 otherCenter = otherTrans.trans(other.getCenter());
				
				// Calculate distance between circles
				Vector2 distanceVector = new Vector2(thisCenter);
				distanceVector.sub(otherCenter);
				float distance = distanceVector.magnitude();
				
				// Check for collision with other circle by comparing distance
				if( distance <= radius+other.getRadius())	{return true;}
				else										{return false;}
		}
		
		/* ----------------------------------------------------------
		 * Render this bounding circle
		 * ----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d, Matrix3 thisTrans, float theta) {
			Vector2 renderingCenter = thisTrans.trans(center);
			int x = (int)(renderingCenter.x-radius);
			int y = (int)(renderingCenter.y-radius);
			_g2d.setColor(color);
			_g2d.drawOval(x,y,(int)radius*2, (int)radius*2);
		}
		
		
	} // End of inner class BoundingCircle
	
	/* -------------------------------------------------------------------------
	 * Inner class BoundingRectangle
	 * -------------------------------------------------------------------------
	 */
	public static class BoundingRectangle extends BoundingGeometry {
		
		public Vector2 halfSize;
		// public Spinor orientation; //?
		
		/* ----------------------------------------------------------
		 * Constructors
		 * ----------------------------------------------------------
		 */
		public BoundingRectangle() {
			center		= new Vector2();
			halfSize	= new Vector2(20.0f, 20.0f);
		}
		
		public BoundingRectangle(Vector2 hs) {
			center		= new Vector2();
			halfSize	= new Vector2(hs);
		}
		
		public BoundingRectangle(float hw, float hh) {
			center		= new Vector2();
			halfSize	= new Vector2(hw,hh);
		}
		
		public BoundingRectangle(BoundingRectangle br) {
			center		= new Vector2(br.center);
			halfSize	= new Vector2(br.halfSize);
		}
		
		/* ----------------------------------------------------------
		 * Getters and Setters
		 * ----------------------------------------------------------
		 */
		public Vector2	getHalfSize()					{return halfSize;}
		public void		setHalfSize(Vector2 v)			{halfSize.set(v);}
		public void		setHalfSize(float x, float y)	{halfSize.set(x,y);}
		public Type		getType()	{return Type.RECTANGLE;}
		
		/* ----------------------------------------------------------
		 * Collision detection
		 * ----------------------------------------------------------
		 */
		public boolean collision(	BoundingGeometry other,
									Matrix3 thisTrans,
									Matrix3 otherTrans) {
			
			/* Rectangle with circle collision */
			if( other.getType() == Type.CIRCLE ) {
				// Cast other object to circle object
				BoundingCircle circle = (BoundingCircle)other;
				return collision(circle,this,otherTrans,thisTrans);
				
			/* Rectangle with rectangle collision */
			} else if(  other.getType() == Type.RECTANGLE ) {
				// Cast other object to rectangle object
				BoundingRectangle otherRect = (BoundingRectangle)other;
				return collision(otherRect,thisTrans,otherTrans);
			}
			
			// Something unexpected has happened
			return false;
			
		}
		
		/* ----------------------------------------------------------
		 * Collide this rectangle with another rectangle
		 * ----------------------------------------------------------
		 */
		public boolean collision(	BoundingRectangle other,
									Matrix3 thisTrans,
									Matrix3 otherTrans) {
			
			float A1, A2, A3, A4, B1, B2, B3, B4;	// Projected corners
			Vector2 minMaxA = new Vector2(), minMaxB = new Vector2();
			
			// Create corners of this rectangle in this objects space
			Vector2 cornerA1 = new Vector2(center.x+halfSize.x,center.y-halfSize.y);
			Vector2 cornerA2 = new Vector2(center.x+halfSize.x,center.y+halfSize.y);
			Vector2 cornerA3 = new Vector2(center.x-halfSize.x,center.y+halfSize.y);
			Vector2 cornerA4 = new Vector2(center.x-halfSize.x,center.y-halfSize.y);
			
			// Create corners of other rectangle in that objects space
			Vector2 oCenter	 = other.getCenter();
			Vector2 oHalfSize= other.halfSize;
			
			Vector2 cornerB1 = new Vector2(oCenter.x+oHalfSize.x,oCenter.y-oHalfSize.y);
			Vector2 cornerB2 = new Vector2(oCenter.x+oHalfSize.x,oCenter.y+oHalfSize.y);
			Vector2 cornerB3 = new Vector2(oCenter.x-oHalfSize.x,oCenter.y+oHalfSize.y);
			Vector2 cornerB4 = new Vector2(oCenter.x-oHalfSize.x,oCenter.y-oHalfSize.y);
			
			// Transform all corners to world space
			thisTrans.transUpdate(cornerA1);  thisTrans.transUpdate(cornerA2);
			thisTrans.transUpdate(cornerA3);  thisTrans.transUpdate(cornerA4);
			otherTrans.transUpdate(cornerB1); otherTrans.transUpdate(cornerB2);
			otherTrans.transUpdate(cornerB3); otherTrans.transUpdate(cornerB4);
			
			// Create the four vectors that make up the test axises.
			Vector2 axisA1 = thisTrans.transDir( new Vector2(1,0));
			Vector2 axisA2 = thisTrans.transDir( new Vector2(0,1));
			Vector2 axisB1 = otherTrans.transDir(new Vector2(1,0));
			Vector2 axisB2 = otherTrans.transDir(new Vector2(0,1));
			
			// Test against first axis
			A1 = cornerA1.dot(axisA1); A2 = cornerA2.dot(axisA1);
			A3 = cornerA3.dot(axisA1); A4 = cornerA4.dot(axisA1);
			B1 = cornerB1.dot(axisA1); B2 = cornerB2.dot(axisA1);
			B3 = cornerB3.dot(axisA1); B4 = cornerB4.dot(axisA1);
			maxMin(A1, A2, A3, A4, minMaxA);
			maxMin(B1, B2, B3, B4, minMaxB);
			if( !axisTest(minMaxA, minMaxB) ) {return false;}
			
			// Test against second axis
			A1 = cornerA1.dot(axisA2); A2 = cornerA2.dot(axisA2);
			A3 = cornerA3.dot(axisA2); A4 = cornerA4.dot(axisA2);
			B1 = cornerB1.dot(axisA2); B2 = cornerB2.dot(axisA2);
			B3 = cornerB3.dot(axisA2); B4 = cornerB4.dot(axisA2);
			maxMin(A1, A2, A3, A4, minMaxA);
			maxMin(B1, B2, B3, B4, minMaxB);
			if( !axisTest(minMaxA, minMaxB) ) {return false;}
			
			// Test against third axis
			A1 = cornerA1.dot(axisB1); A2 = cornerA2.dot(axisB1);
			A3 = cornerA3.dot(axisB1); A4 = cornerA4.dot(axisB1);
			B1 = cornerB1.dot(axisB1); B2 = cornerB2.dot(axisB1);
			B3 = cornerB3.dot(axisB1); B4 = cornerB4.dot(axisB1);
			maxMin(A1, A2, A3, A4, minMaxA);
			maxMin(B1, B2, B3, B4, minMaxB);
			if( !axisTest(minMaxA, minMaxB) ) {return false;}
			
			// Test against fourth axis
			A1 = cornerA1.dot(axisB2); A2 = cornerA2.dot(axisB2);
			A3 = cornerA3.dot(axisB2); A4 = cornerA4.dot(axisB2);
			B1 = cornerB1.dot(axisB2); B2 = cornerB2.dot(axisB2);
			B3 = cornerB3.dot(axisB2); B4 = cornerB4.dot(axisB2);
			maxMin(A1, A2, A3, A4, minMaxA);
			maxMin(B1, B2, B3, B4, minMaxB);
			if( !axisTest(minMaxA, minMaxB) ) {return false;}
			
			return true;
		}
		
		/* ----------------------------------------------------------
		 * Axis test, true means intersection along axis
		 * ----------------------------------------------------------
		 */
		public boolean axisTest(Vector2 minMaxA, Vector2 minMaxB) {
			if(minMaxB.x <= minMaxA.y && minMaxA.x < minMaxB.y)	{return true;}
			if(minMaxA.x <= minMaxB.y && minMaxB.x < minMaxA.y) {return true;}
			else												{return false;}
		}
		
		/* ----------------------------------------------------------
		 * Finds max and min
		 * ----------------------------------------------------------
		 */
		public void maxMin(float a, float b, float c, float d, Vector2 minMax) {
			float min1, min2, max1, max2;
			if(a<=b)		{min1=a; max1=b;}	else {min1=b; max1=a;}
			if(c<=d)		{min2=c; max2=d;}	else {min2=d; max2=c;}
			
			if(min1<=min2)	{minMax.x=min1;}	else {minMax.x=min2;}
			if(max1>=max2)	{minMax.y=max1;}	else {minMax.y=max2;}
		}
		
		/* ----------------------------------------------------------
		 * Rendering
		 * ----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d, Matrix3 thisTrans, float theta) {
			AffineTransform transformCopy	= _g2d.getTransform();
			AffineTransform transform		= _g2d.getTransform();
			
			_g2d.setColor(color);
			
			Vector2 pos = thisTrans.trans(center);
			
			// For drawing rectangles at correct position
			int drawX	= (int)(pos.x - halfSize.x);
			int drawY	= (int)(pos.y - halfSize.y);
			
			// For rotating
			int centerX	= (int)(pos.x);
			int centerY	= (int)(pos.y);
			
			// transform before rendering
			transform.rotate(theta, centerX, centerY); // Rotate around center
			_g2d.setTransform(transform); // Apply transform
			_g2d.drawRect(drawX, drawY, (int)halfSize.x*2, (int)halfSize.y*2);
			
			// Restore the transform
			_g2d.setTransform(transformCopy);
		}
	} // End of inner class BoundingRectangle
		
	/* -------------------------------------------------------------------------
	 * Do collision detection by recieving an array/list with all actors/bodies
	 * in the game and loop through it to see which are colliding.
	 * Making sure pairs of objects are only checked once.
	 * -------------------------------------------------------------------------
	 */
	public void buildContactList(ArrayList bodies) {
		
		int length = bodies.size();
		int currentPos = 0;
		RigidBody		currentBody, otherBody;
		BoundingGeometry	currentGeom, otherGeom;
		Matrix3				currentTrans, otherTrans;
		
		// Iterate through list and generate possible contacts
		for(int j=0;j<length;j++) {
			// Retrieve information about the current body being tested
			currentBody = (RigidBody)bodies.get(j);
			currentGeom	 = currentBody.getBoundingGeometry();
			currentTrans = currentBody.getTransform();
			
			// Inner loop
			for(int i=j+1;i<length;i++) {
				// Retrieve information about the other body
				otherBody = (RigidBody)bodies.get(i);
				otherGeom  = otherBody.getBoundingGeometry();
				otherTrans = otherBody.getTransform();
				
				// Test for contact
				if(currentGeom != null && otherGeom != null)
				if( !(currentBody.isStatic()&&otherBody.isStatic()))
				if(currentGeom.collision(otherGeom, currentTrans, otherTrans)) {
					potentialCollisions.add(
							new PotentialContact(currentBody,otherBody));
					currentNrOfCollisions++;
				}
			} // End of inner loop over "i"
		} // End of loop over "j"
		
	}
	
	/* -------------------------------------------------------------------------
	 * Clears the current list of potential collisions from entries and resets
	 * the counter.
	 * -------------------------------------------------------------------------
	 */
	public void clear() {
		int length = potentialCollisions.size();
		PotentialContact current;
		
		for(int i=0;i<length;i++) {
			current = (PotentialContact)potentialCollisions.get(i);
			current.clear();
		}
		
		potentialCollisions.clear();
		currentNrOfCollisions = 0;
	}
	
	/* -------------------------------------------------------------------------
	 * Returns the current list of potential contacts.
	 * -------------------------------------------------------------------------
	 */
	public ArrayList getContactList() {return potentialCollisions;}
	
} // End of class CoarseCollision
