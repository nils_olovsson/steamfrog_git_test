/*
 * ContactResolver.java
 * Recieves a list of actuall contacts that is looped through once. The contacts
 * are then solved in the order of their occurance in the loop.
 * First the bodies involved are given a new velocity through an impulse and
 * then they are separeted by a linear projection outwards from each other along
 * the contact normal.
 * 
 * NOTE: 
 * 
 * TODO: Replace current linear projection for interpenetration resolution with 
 *		 a non-linear projection. Two bodies with infinite mass but finite
 *		 inertia may rotate into one an other without being able to separate
 *		 during interpenetration solving.
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.util.ArrayList;
import physics.Math.Vector2;

public class ContactResolver {
	
	/* -------------------------------------------------------------------------
	 * Loops through all contacts and solves them
	 * -------------------------------------------------------------------------
	 */
	public void resolveContacts(ArrayList contacts, float dt) {
		
		int length = contacts.size();
		NarrowCollision.Contact current;
		
		for(int i=0;i<length;i++) {
			current = (NarrowCollision.Contact)contacts.get(i);
			if(current.rb1 != null && current.rb2 != null) {
				current.calculateInternals(dt);
				resolveVelocity(current);
				resolveInterpenetration(current);
			}
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Moves objects away from each other so that they no longer overlap
	 * -------------------------------------------------------------------------
	 */
	public void resolveInterpenetration(NarrowCollision.Contact contact) {
	
		RigidBody rb1 = contact.rb1;
		RigidBody rb2 = contact.rb2;
		Vector2 normal = contact.contactNormal;
		float penetration = contact.penetration;
		if(penetration <= 0.0f) {return;}
		
		float totalInverseMass = rb1.inverseMass + rb2.inverseMass;
		if(totalInverseMass == 0.0f) {return;}
		
		if( !rb1.hasFiniteMass() ) {
			rb2.pos.add(normal.multNew(penetration));
		} else if( !rb2.hasFiniteMass() ) {
			rb1.pos.sub(normal.multNew(penetration));
		} else {
			rb1.pos.sub(normal.multNew(penetration*rb1.inverseMass/totalInverseMass));
			rb2.pos.add(normal.multNew(penetration*rb2.inverseMass/totalInverseMass));
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Changes linear and angular velocity by applying proper impule
	 * 
	 * -------------------------------------------------------------------------
	 */
	public void resolveVelocity(NarrowCollision.Contact contact) {
		
		// Just extract some references and values
		Vector2		relContactPos1			= contact.relContactPos1;
		Vector2		relContactPos2			= contact.relContactPos2;
		RigidBody	rb1						= contact.rb1;
		RigidBody	rb2						= contact.rb2;
		float		friction				= contact.friction;
		float		desiredDeltaVelocity	= contact.desiredDeltaVel;
		float		deltaVelocity			= contact.deltaVelocity;
		Vector2		contactVelocity			= contact.contactVelocity;
		
		// Initialize stuff
		Vector2 impulse;
		Vector2 velocityChange;
		float impulsiveTorque;
		float rotationChange;
		
		if(friction == 0.0f || Math.abs(contactVelocity.y) < 1.0f) {
			// Build friction less impulse
			impulse = new Vector2(desiredDeltaVelocity/deltaVelocity, 0.0f);
			contact.contact2World.transUpdate(impulse);
		} else {
			// Build impulse with friction
			impulse = calculateFrictionImpulse(contact);
			contact.contact2World.transUpdate(impulse);
		}
		
		if(!rb1.isStatic()) {
			// Calcuate changes in velocity by impulse for object 1
			velocityChange	= impulse.multNew(rb1.getInverseMass());
			impulsiveTorque	= relContactPos1.cross(impulse);
			rotationChange	= impulsiveTorque*rb1.getInverseInertia();
			rb1.vel.add(velocityChange);
			rb1.rotation += rotationChange;
		}
		
		// Calcuate changes in velocity by impulse for object 2
		if( rb2 != null && !rb2.isStatic()) {
			impulse.invert();
			velocityChange	= impulse.multNew(rb2.getInverseMass());
			impulsiveTorque = relContactPos2.cross(impulse);
			rotationChange	= impulsiveTorque*rb2.getInverseInertia();
			rb2.vel.add(velocityChange);
			rb2.rotation += rotationChange;
		}

	}
	
	/* -------------------------------------------------------------------------
	 * Build impulse with friction
	 * -------------------------------------------------------------------------
	 */
	public Vector2 calculateFrictionImpulse(NarrowCollision.Contact contact) {
		// Just extract some references and values
		float		friction				= contact.friction;
		float		desiredDeltaVelocity	= contact.desiredDeltaVel;
		float		deltaVelocity			= contact.deltaVelocity;
		Vector2		contactVelocity			= contact.contactVelocity;
		
		// Build impulse
		Vector2 impulse = new Vector2(	desiredDeltaVelocity/deltaVelocity, 
										-contactVelocity.y);
		
		// Is impulse static or dynamic? If static, it needs to be scaled down
		float planarImpulse = Math.abs(impulse.y);
		if( planarImpulse > Math.abs(impulse.x)*friction ) {
			impulse.y /= planarImpulse;
			impulse.y *= -impulse.x*friction;
			
		}
		
		return impulse;
	}
}
