/*
 * ForceRegistry.java
 * A registry that pairs ForceGenerators together with RigidBodies.
 * One ForceGenerator may be connected to several RigidBodies and one RigidBody
 * may be connected to several ForceGenerators. The registry itsel is a List.
 * From here forces are also updated/applied to RigidBodies.
 * 
 * Also has a rendering functionality that may render all forces.
 * 
 * Contains one inner class for a force registration pair of one RigidBody and
 * one ForceGenerator.
 * 
 * 
 * NOTE:
 * TODO: Nothing!
 * 
 * Author: Nils Olofsson
 * Last Revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.awt.Graphics2D;
import java.util.ArrayList;

public class ForceRegistry {
	
	protected ArrayList registrations; // Holds list of registrations
	
	/* -------------------------------------------------------------------------
	 * Keeps track of one force generator and the particle it applies to
	 * -------------------------------------------------------------------------
	 */
	protected class ForceRegistration {
		RigidBody		body;
		ForceGenerator	fg;
		public RigidBody		getBody()	{return body;}
		public ForceGenerator	getForce()	{return fg;}
	}
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
	public ForceRegistry() {
		registrations = new ArrayList<ForceRegistration>();
	}
	
	
	/* -------------------------------------------------------------------------
	 * Registers the given force generator to apply to the given particle
	 * -------------------------------------------------------------------------
	 */
	public void add(RigidBody body, ForceGenerator fg) {
		ForceRegistration registration = new ForceRegistration();
		registration.body = body;
		registration.fg = fg;
		registrations.add(registration);
	}
	
	/* -------------------------------------------------------------------------
	 * Removes the given registered pair from the registry.
	 * If pair is not registered, this will have no effect.
	 * -------------------------------------------------------------------------
	 */
	public void remove(RigidBody body, ForceGenerator fg) {
		
		// Iterate through list and compare objects...
		ForceRegistration currentFR;	// Container for the current entry
		int length = registrations.size();
		
		/* Loops through all entries in the registry using the iterator */
		for(int i=0;i<length;i++) {
			currentFR = (ForceRegistration)registrations.get(i);
			if(currentFR.fg == fg && currentFR.body == body) {
				registrations.remove(currentFR); break;
			} // End of if
		} // End of while
	}
	
	/* -------------------------------------------------------------------------
	 * Removes all entries in register where this particle is present
	 * -------------------------------------------------------------------------
	 */
	public void remove(RigidBody body) {
		ArrayList removeRegister = new ArrayList<ForceRegistration>();
		ForceRegistration currentFR; // Container for the current entry
		int length = registrations.size();
		int i;
		
		// Loops through all entries in the registry using the iterator
		for(i=0;i<length;i++) {
			currentFR = (ForceRegistration)registrations.get(i);
			if(	currentFR.body == body ||
				currentFR.fg.containsInside(body)) {
				removeRegister.add(currentFR);
			}
		}
		
		// Go trhough all entries registered for removal and remove them
		length = removeRegister.size();
		for(i=0; i<length;i++) {
			currentFR = (ForceRegistration)removeRegister.get(i);
			registrations.remove(currentFR);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Removes all entries in register where this ForceGenerator is present
	 * -------------------------------------------------------------------------
	 */
	public void remove(ForceGenerator fg) {
		ArrayList removeRegister = new ArrayList<ForceRegistration>();
		ForceRegistration currentFR; // Container for the current entry
		int length = registrations.size();
		int i;
		
		// Loops through all entries in the registry using the iterator
		for(i=0;i<length;i++) {
			currentFR = (ForceRegistration)registrations.get(i);
			if(	currentFR.fg == fg ) {removeRegister.add(currentFR);}
		}
		
		// Go through all entries registered for removal and remove them
		length = removeRegister.size();
		for(i=0; i<length;i++) {
			currentFR = (ForceRegistration)removeRegister.get(i);
			registrations.remove(currentFR);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Clears all registrations from the registry.
	 * This will not delete the bodies or the force generators themselves,
	 * just the records of thier connection.
	 * -------------------------------------------------------------------------
	 */
	void clear(){registrations.clear();}
	
	/* -------------------------------------------------------------------------
	 * Calls all the force generators to update the forces of their
	 * corresponding particles
	 * -------------------------------------------------------------------------
	 */
	void updateForces(float dt) {
		
		ForceRegistration currentPFR;	// Container for the current entry
		int length = registrations.size();
		
		/* Loops through all entries in the registry using the iterator */
		for(int i=0;i<length;i++) {
			currentPFR = (ForceRegistration)registrations.get(i);
			currentPFR.fg.updateForce(currentPFR.body, dt);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Renders the forces and it's connections to bodies in the registry
	 * -------------------------------------------------------------------------
	 */
	public void render(Graphics2D _g2d) {
		int length =	registrations.size();
		ForceRegistration	currentFR;
		RigidBody		currentBody;
		ForceGenerator	currentFG;
		
		for(int i=0; i<length; i++) {
			currentFR = (ForceRegistration)registrations.get(i);
			currentFG = currentFR.getForce();
			currentFG.render(_g2d, currentFR.getBody());
		}
	}

}
