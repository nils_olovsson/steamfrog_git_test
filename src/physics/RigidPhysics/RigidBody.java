/*
 * RigidBody.java
 * The RigidBody contains various properties for linear and angular movement. 
 * It also contains a bounding geometry used for coarse collision check as well
 * as a contact geometry used to check for narrow/true collisions. The contact
 * geometries are the ones that actually are in contact when two bodies/objects
 * collide. Both these geometries can be rendered from the RigidBody by calling
 * rendering functions that will in turn call the rendering functions for these
 * geometries.
 * Contact check and resolution is treated in separate classes.
 * 
 * This class also contains an integration function that applies the accumulated
 * forces for this timestep to move and rotate the RigidBody.
 * Forces are applied from ForceGenerators.
 * 
 * NOTE: Sleep properties are not implemented.
 * 
 * TODO: Nothing! (Maybe some restructuring...)
 * 
 * Author: Nils Olofsson
 * Last revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import physics.Math.Matrix3;
import physics.Math.Spinor;
import physics.Math.Vector2;
import physics.RigidPhysics.CoarseCollision.*;
import physics.RigidPhysics.NarrowCollision.*;

public class RigidBody {
	
	/* Liniear properties */
	protected float inverseMass;		// Inverse mass
	protected Vector2 pos;				// Position in 2D space
	protected Vector2 vel;				// Velocity in 2D space
	protected float linearDamping;		// Damping for numerical instability
	
	/* Angular Properties */
	protected float inverseInertia;		// Inverse Inertia around CoM
	protected Spinor orientation;		// Orientation in 2D space
	protected float rotation;			// Angular velocity in 2D space
	protected float angularDamping;		// Damping for numerical instability
	
	protected Matrix3 transformMatrix;	// Convert body space to world space
										// (and vice versa).
	
	/* "Bounciness" and "stickieness" */
	protected float restitution = 0.2f;
	protected float friction  = 1.0f;
	
	/* Accumulators */
	protected Vector2 forceAccum;		// Accumulates force during one update
	protected float torqueAccum;		// Accumulates torque during one update
	protected Vector2 acceleration;		// Can hold a constant acceleration for
										// an object, e.g gravity.
	protected Vector2 lastFrameAcc;		// Acceleration during last update.
	
	/* Sleep */
	protected float		motion;			// Holds amount of motion of body
	protected boolean	isAwake;		// Can be put to sleep to avoid it being updated
	protected boolean	canSleep;		// Some bodies may not be allowed to sleep
	
	/* Collisions and contatcs */
	//protected static CoarseCollision ccInstance = new CoarseCollision(); // Instance used for creating geometry
	//protected static NarrowCollision ncInstance = new NarrowCollision(); // Instance used for creating geometry
	protected BoundingGeometry boundingGeom;	// One bounding geometry
	protected ContactGeometry  contactGeom;		// One contact geometry
			
	/* -------------------------------------------------------------------------
	 * Constructors
	 * -------------------------------------------------------------------------
	 */
	public RigidBody() {
		inverseMass		= 1.0f;
		pos				= new Vector2();	// [0.0f,0.0f]
		vel				= new Vector2();	// [0.0f,0.0f]
		linearDamping	= 0.9999f;
		
		inverseInertia	= 0.001f;
		orientation		= new Spinor();	// Facing forwards along x-axis
		rotation		= 0.0f;
		angularDamping	= 0.999f;
		
		forceAccum		= new Vector2();	// [0.0f,0.0f]
		torqueAccum		= 0.0f;
		acceleration	= new Vector2();	// [0.0f,0.0f]
		lastFrameAcc	= new Vector2();	// [0.0f,0.0f]
		
		transformMatrix = new Matrix3();
		
		// Calculate transform matrix, and normalize orientaion and maybe do
		// something more...
		calculateDerivedData();
	}
	
	public RigidBody(RigidBody rb) {		
		inverseMass		= rb.inverseMass;
		pos				= new Vector2(rb.pos);
		vel				= new Vector2(rb.vel);
		linearDamping	= rb.linearDamping;
		
		inverseInertia	= rb.inverseInertia;
		orientation		= new Spinor(rb.orientation);
		rotation		= rb.rotation;
		angularDamping	= rb.angularDamping;
		
		forceAccum		= new Vector2();
		torqueAccum		= 0.0f;
		acceleration	= new Vector2();
		lastFrameAcc	= new Vector2();
		
		transformMatrix = new Matrix3(rb.transformMatrix);
		
		boundingGeom = null; contactGeom = null;
		
		if(rb.boundingGeom instanceof BoundingCircle) {
			boundingGeom = new BoundingCircle((BoundingCircle)rb.boundingGeom);
		} else if(rb.boundingGeom instanceof BoundingRectangle) {
			boundingGeom = new BoundingRectangle((BoundingRectangle)rb.boundingGeom);
		}
		
		if(rb.contactGeom instanceof ContactCircle) {
			contactGeom = new ContactCircle((ContactCircle)rb.contactGeom);
		} else if(rb.contactGeom instanceof ContactRectangle) {
			contactGeom = new ContactRectangle((ContactRectangle)rb.contactGeom);
		}

	}
	
	public RigidBody(float x, float y, float halfWidth, float halfHeight) {
		inverseMass		= 1.0f;
		pos				= new Vector2();	// [0.0f,0.0f]
		vel				= new Vector2();	// [0.0f,0.0f]
		linearDamping	= 0.999f;
		
		inverseInertia	= 0.001f;
		orientation		= new Spinor();	// Facing forwards along x-axis
		rotation		= 0.0f;
		angularDamping	= 0.99f;
		
		forceAccum		= new Vector2();	// [0.0f,0.0f]
		torqueAccum		= 0.0f;
		acceleration	= new Vector2();	// [0.0f,0.0f]
		lastFrameAcc	= new Vector2();	// [0.0f,0.0f]
		
		transformMatrix = new Matrix3();
		
		// Calculate transform matrix, and normalize orientaion and maybe do
		// something more...
		calculateDerivedData();
		
		this.setPosition(x,y);
		this.buildContactGeometry(halfWidth, halfHeight);
	}
	
	/* -------------------------------------------------------------------------
	 * Getters and setters
	 * -------------------------------------------------------------------------
	 */
	// Mass
	public void	setMass(float m)			{inverseMass = 1/m;}
	public float   getMass()				{return 1/inverseMass;}
	public void    setInverseMass(float iM)	{inverseMass = iM;}
	public float   getInverseMass()			{return inverseMass;}
	public  boolean hasFiniteMass()			{return inverseMass>0.0f;}
	// Inertia
	public void	 setInertia(float in)		{inverseInertia = 1.0f/in;}
	public float getInertia()				{return 1.0f/inverseInertia;}
    public void	 setInverseInertia(float iIn){inverseInertia = iIn;}
    public float getInverseInertia()		{return inverseInertia;}
	// Damping
	public void	 setDamping(float l, float a){linearDamping=l;angularDamping=a;}
	public void	 setLinearDamping(float ld)		{linearDamping = ld;}
	public float getLinearDamping()				{return linearDamping;}
	public void	 setAngularDamping(float ad)	{angularDamping = ad;}
	public float getAngularDamping()			{return angularDamping;}
	// Position
	public void		setPositionX(float _x)		{pos.x = _x;}
	public void		setPositionY(float _y)		{pos.y = _y;}
	public void		setPosition(Vector2 p)		{pos.set(p);}
	public void		setPosition(float x,float y){pos.set(x,y);}
	public void		getPosition(Vector2 p)		{p.set(pos);}
	public Vector2	getPosition()				{return new Vector2(pos);}
	// Orientation
	public void	setOrientation(Spinor s)		{orientation.set(s);}
	public void setOrientation(float a,float b)	{orientation.set(a,b);}
	public void	getOrientation(Spinor s){s.set(orientation);}
	public Spinor	getOrientation()	{return new Spinor(orientation);}
	public float	getOrientationRad()	{return orientation.getOrientation();}
	public float	getOrientationDeg()	{return orientation.getOrientationDeg();}
	// Transform
	public void getTransform(Matrix3 trans)	{trans.set(transformMatrix);}
	//public Matrix3 getTransform()			{return new Matrix3(transformMatrix);}
	public Matrix3 getTransform()			{return transformMatrix;}
    // Velocity
	public void	setVelocityX(float x)		{vel.setX(x);}
	public void	setVelocityY(float y)		{vel.setY(y);}
	public void	setVelocity(Vector2 v)		{vel.set(v);}
	public void	setVelocity(float x,float y){vel.set(x,y);}
	public void	getVelocity(Vector2 v)		{v.set(vel);}
	public Vector2 getVelocity()			{return new Vector2(vel);}
	public void	addVelocity(Vector2 deltaV)	{vel.add(deltaV);}
	// Rotation (aka Angular velocity)
	public void	 setRotation(float rot)		{rotation = rot;}
	public float getRotation()				{return rotation;}
	public void	 addRotation(float deltaRot){rotation+=deltaRot;}
	// Restitution and friction
	public void setRestitution(float f)		{restitution = f;}
	public float getRestitution()			{return restitution;}
	public void setFriction(float f)		{friction = f;}
	public float getFriction()				{return friction;}
	
	// Acceleration
	public void setAcceleration(Vector2 acc)	{acceleration.set(acc);}
	public void setAcceleration(float x,float y){acceleration.set(x,y);}
	public void getAcceleration(Vector2 acc)	{acc.set(acceleration);}
	public Vector2 getAcceleration()			{return new Vector2(acceleration);}
	public void	getLastFrameAcc(Vector2 acc)	{acc.set(lastFrameAcc);}
	public Vector2 getLastFrameAcc()			{return new Vector2(lastFrameAcc);}
	
	public void setStatic()		{inverseMass = 0.0f; inverseInertia=0.0f;}
	public boolean isStatic()	{return inverseMass ==0.0f && inverseInertia == 0.0f;}
	
	/* -------------------------------------------------------------------------
	 * Bounding geometry
	 * -------------------------------------------------------------------------
	 */
	public void setBoundingGeometry(CoarseCollision.BoundingGeometry g) {
		boundingGeom=g;
	}
	public void setBoundingCircle(float r) {
		boundingGeom = new BoundingCircle(r);
	}
	public void setBoundingRectangle(float hw, float hh) {
		boundingGeom = new BoundingRectangle(hw, hh);
	}
	
	public CoarseCollision.BoundingGeometry getBoundingGeometry(){
		return boundingGeom;
	}
	
	/* -------------------------------------------------------------------------
	 * Contact geometry
	 * -------------------------------------------------------------------------
	 */
	public void setContactGeometry(NarrowCollision.ContactGeometry g) {
		g.body = this;
		contactGeom=g;
	}
	public void setContactCircle(float r) {
		contactGeom = new NarrowCollision.ContactCircle(this, r);
	}
	public void setContactRectangle(float hw, float hh) {
		contactGeom = new NarrowCollision.ContactRectangle(this, hw, hh);
	}
	
	/* -------------------------------------------------------------------------
	 * Builds boundary and contact geometry
	 * -------------------------------------------------------------------------
	 */
	public void buildContactGeometry(float hw, float hh) {
		contactGeom = new NarrowCollision.ContactRectangle(this, hw, hh);
		if(hw == hh || Math.max(hw, hh)<200.0f) {
			boundingGeom = new BoundingCircle((float)Math.sqrt(hw*hw+hh*hh));
		} else {
			boundingGeom = new BoundingRectangle(hw, hh);
		}
	}
	
	public void setNoContactGeometry() {
		boundingGeom = null;
		contactGeom = null;
	}
	
	/* -------------------------------------------------------------------------
	 * Forces, torques and accumulators
	 * -------------------------------------------------------------------------
	 */
	public void	clearAccumulators()		{torqueAccum=0.0f; forceAccum.clear();}
	public void	addForce(Vector2 force)	{forceAccum.add(force); isAwake=true;}
	public void	addTorque(float torque)	{torqueAccum += torque;}
	
	// Add force on object at point in world coordinates
	public void	addForceAtPoint(Vector2 force, Vector2 point) {
		// Convert to coordinate relative to center of mass
		Vector2 pt = new Vector2(point);
		pt.sub(pos);
		
		forceAccum.add(force);
		torqueAccum += pt.cross(force);
	}
	
	// Add force on object at point in object coordinates
	public void	addForceAtBodyPoint(Vector2 force, Vector2 point) {
		// Convert to coordinates relative to center of mass
		Vector2 pt = getPointInWorldSpace(point);
		addForceAtPoint(force, pt);
		isAwake = true;
	}
	
	// Makes sure a max speed and rotation is not exceeded
	public void applySpeedRotationConstraints() {
		float speedLimit = 400.0f;
		float rotatLimit = 10f;
		
		// Speed
		if(vel.sqrdMagnitude() > speedLimit*speedLimit) {
			vel.normalize(); vel.mult(speedLimit);
		}
		
		// Rotation
		if		(rotation > rotatLimit) {rotation=rotatLimit;}
		else if	(rotation < -rotatLimit){rotation=-rotatLimit;}
	}
	
	/* -------------------------------------------------------------------------
	 * Transforming functions
	 * -------------------------------------------------------------------------
	 */
	/* Give point in world space, return in object space*/
	public Vector2 getPointInLocalSpace(Vector2 p) {
		return transformMatrix.invTrans(p);
	}
	
	// Give point in object space, return in world space
	public Vector2 getPointInWorldSpace(Vector2 p) {
		return transformMatrix.trans(p);
	}
	
	// Give direction in world space, return in object space
	public Vector2 getDirectionInLocalSpace(Vector2 d) {
		return transformMatrix.invTransDir(d);
	}
	
	// Give direction in object space, return in world space
	public Vector2 getDirectionInWorldSpace(Vector2 d) {
		return transformMatrix.transDir(d);
	}
	
	/* -------------------------------------------------------------------------
	 * Awake functions...Maybe not needed, can probably improve performance
	 * -------------------------------------------------------------------------
	 */
	public boolean	getAwake()				{return isAwake;}
	public void		setAwake(boolean a)		{isAwake = a;}
	public boolean	getCanSleep()			{return canSleep;}
	public void		setCanSleep(boolean a)	{canSleep = a;}
	
	/* -------------------------------------------------------------------------
	 * Create the transform matrix for transforming body space to world space
	 * (The inverse matrix gives world space to body space transform)
	 * -------------------------------------------------------------------------
	 */
	public static void _calculateTransformMatrix(	Matrix3 tm,
													Vector2 _position,
													Spinor _orientation) {
		float theta = _orientation.getOrientation();
		Matrix3 m = new Matrix3();
		
		// Create the transform matrix
		m.data[0] = (float)Math.cos(theta); m.data[1] = -(float)Math.sin(theta);
		m.data[2] = _position.x;
		m.data[3] = (float)Math.sin(theta); m.data[4] =  (float)Math.cos(theta);
		m.data[5] = _position.y;
		m.data[6] = 0.0f; m.data[7] = 0.0f; m.data[8] = 1.0f;
		
		tm.set(m);
	}
			
	/* -------------------------------------------------------------------------
	 * Calculates internal data from internal state data. This should be called 
	 * after the bodys state is altered directly (called automatically during
	 * integration). If you change the bodys state and then tend to integrate
	 * before quering any data (such as the transform matrix) you can omit this
	 * step.
	 * -------------------------------------------------------------------------
	 */
	public void calculateDerivedData() {
		orientation.normalize();
		_calculateTransformMatrix(transformMatrix, pos, orientation);
	}
	
	/* -------------------------------------------------------------------------
	 * Integrate one step using delta t
	 * -------------------------------------------------------------------------
	 */
	public void integrate(float dt) {
		// Calculate linear acceleration from force inputs
		lastFrameAcc.set(acceleration);	
		lastFrameAcc.addScaled(forceAccum, inverseMass);
		
		// Calculate angular acceleration from torque inputs
		float angularAcc = inverseInertia*torqueAccum;
		
		// Update linear velocity from both acceleration and impulse
		vel.addScaled(lastFrameAcc, dt);
		
		// Update angular velocity from both acceleration and impulse
		rotation += angularAcc*dt;
		
		// Impose drag
		vel.mult(linearDamping);
		rotation*=angularDamping;
		
		// Make sure speedlimit is not exceeded
		//applySpeedRotationConstraints();
		// Adjust positions
		pos.addScaled(vel, dt);
		orientation.multScaled(rotation, dt);
		
		// Calculate new transform matrix and normalize orientation
		calculateDerivedData();
		
		clearAccumulators();
		
	} // End of integrate
	
	/* -------------------------------------------------------------------------
	 * Render the bounding geometry associated with this rigid body
	 * -------------------------------------------------------------------------
	 */
	public void renderBoundingGeom(Graphics2D _g2d) {
		if(boundingGeom != null) {
			boundingGeom.render(
				_g2d,
				transformMatrix,
				orientation.getOrientation()
				);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Render the bounding geometry associated with this rigid body
	 * -------------------------------------------------------------------------
	 */
	public void renderContactGeom(Graphics2D _g2d) {
		if(contactGeom != null) {
			contactGeom.render(_g2d);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Render som information on screen about this body
	 * -------------------------------------------------------------------------
	 */
	public void drawInfo(Graphics2D _g2d, int x, int y) {
			_g2d.setColor(Color.RED);
			_g2d.setFont( new Font( "Courier New", Font.PLAIN, 12 ) );
			
			_g2d.drawString( String.format( "Position: %f, %f",
					pos.x, pos.y),	x, y );
			_g2d.drawString( String.format( "Velocity: %f, %f",
					vel.x, vel.y),	x, y+20);
			_g2d.drawString( String.format( "Orientation: %f",
					getOrientationRad()),	x, y+40);
	}
}
