/*
 * NarrowCollision.java
 * Used to generate contact data between bodies if they are in contact/are
 * colliding. This class builds and contains a list of actuall contacts that
 * can be sent to the contact solver.
 * 
 * This file contains four inner public classes:
 *		Contact - Contains and calculates various contact information
 *		ContactGeometry (Abstract class)
 *		ContactCircle	 - 
 *		ContactRectangle - 
 * 
 * NOTE: There are some things that is probably no longer used that is left
 *		 within the code. For example the collision counters and maybe some
 *		 aspects/uses of the TYPE enum such as the type variable in Contact.
 * 
 * TODO: Nothing! (Maybe some restructuring...)
 * 
 * Author: Nils Olofsson
 * Last revised: 07/08/2012
 */

package physics.RigidPhysics;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.util.ArrayList;
import physics.Math.Matrix3;
import physics.Math.Vector2;

public class NarrowCollision {
	
	public ArrayList contacts;
	public int maxNrOfCollisions;
	public int currentNrOfCollisions;
	
	/* Enumerated geometry type for coarse collision */
	public enum Type {CIRCLE, RECTANGLE}
	
	/* -------------------------------------------------------------------------
	 * Constructor
	 * -------------------------------------------------------------------------
	 */
	public NarrowCollision() {
		contacts = new ArrayList<Contact>();
		currentNrOfCollisions = 0;
	}
	
	/* -------------------------------------------------------------------------
	 * Inner class contact, contains all data necessery to solve a
	 * collision/contact and interpenetration.
	 * Restitution and friction(?) is given by objects uring solving.
	 * -------------------------------------------------------------------------
	 */
	public static class Contact {
		public RigidBody	rb1;			// Reference to body that provides normal
		public RigidBody	rb2;			// Reference to the other body
		public Vector2		contactPoint;	// Contact position in world coordinates
		public Vector2		contactNormal;	// Direction of contact in world coordinates
		public float		penetration;	// Depth of penetration
		public float		restitution;	// Determines size of tangential impulse
		public float		friction;		// Determines size of perpendicular impulse
		public Matrix3		contact2World;	// Trans from contact space to world space
		public Vector2		contactVelocity;// Closing velocity at point of contact
		public float		desiredDeltaVel;// Required change in velocity to solve contact
		public float		deltaVelocity;	// 
		public Vector2		relContactPos1;	// Position of contact relative to center
		public Vector2		relContactPos2;	// of body one and two.
		
		int type=0; // 0="normal", 1=circle-circle 2=parallell rectanglesa
		
		/* ----------------------------------------------------------
		 * Constructor
		 * ----------------------------------------------------------
		 */
		public Contact() {
			rb1 = null; rb2 = null;
			contactPoint = null; contactNormal = null;
			penetration = 0.0f;
			restitution = 0.2f;
			contact2World = null;
		}
		
		/* ----------------------------------------------------------
		 * Prepare this contact by calculating some internal values
		 * after the contact has been created and registered
		 * ----------------------------------------------------------
		 */
		public void calculateInternals(float dt) {
			contactNormal.normalize();
			// If first body is NULL, swap bodies and invert normal
			if(rb1 == null) {swapBodies();}
			
			// Calculate initial values of restitution and friction
			calculateInitialRestitutionFriction();
			
			// Calculate a set of axes at the contact point
			calculateContactBasis();
			
			// Store the relative position of the contact relative to each body
			relContactPos1 = contactPoint.subNew(rb1.pos);
			if(rb2 != null) {relContactPos2 = contactPoint.subNew(rb2.pos);}
			
			// Find the relative velocity of the bodies at the contact point
			contactVelocity = calculateLocalVelocity(1,dt);
			if(rb2!=null){contactVelocity.sub(calculateLocalVelocity(2,dt));}
			
			// Calculate the desired change in velocity for resolution
			calculateDesiredDeltaVelocity(dt);
			
			// Calculate a delta v...
			calculateDeltaVelocity();
			
		}
		
		/* ----------------------------------------------------------
		 * Swap bodies and inverts normal
		 * ----------------------------------------------------------
		 */
		public void swapBodies() {
			contactNormal.invert();
			RigidBody temp = rb1; rb2 = rb1; rb2 = temp;
		}
		
		/* ----------------------------------------------------------
		 * Calculate initial values of restitution and friction from
		 * values inside bodies
		 * Values are calculated by weighting against higher value.
		 * ----------------------------------------------------------
		 */
		public void calculateInitialRestitutionFriction() {
			float minRest, maxRest, minFric, maxFric;
			float thisRestitution	= rb1.restitution;
			float thisFriction		= rb1.friction;
			
			// If body 2 is not null
			if(rb2 != null) {
				// Find min/max restitution
				if(rb1.restitution < rb2.restitution) {
					minRest = rb1.restitution; maxRest = rb2.restitution;
				} else {
					minRest = rb2.restitution; maxRest = rb1.restitution;
				}
				// Find min/max friction
				if(rb1.friction < rb2.friction) {
					minFric = rb1.friction; maxFric = rb2.friction;
				} else {
					minFric = rb2.friction; maxFric = rb1.friction;
				}
				
				// Calculate weighted values
				thisRestitution = (minRest+2.0f*maxRest)/3.0f;
				thisFriction	= (minFric+2.0f*maxFric)/3.0f;
			}
			
			// Set values
			restitution = thisRestitution;
			friction	= thisFriction;
		}
		
		/* ----------------------------------------------------------
		 * Calculate basis matrix, builds a transformation matrix
		 * that goes from contact space to world space
		 * Contact space is built from contact point and normal
		 * NOTE: Does not have any translation component, only
		 * direction. Can be inverted by transposing, which is fast.
		 * ----------------------------------------------------------
		 */
		public void calculateContactBasis() {
			Vector2 axis2 = contactNormal.perpendicular();
			contact2World = new Matrix3();
			contact2World.data[0] = contactNormal.x;
			contact2World.data[3] = contactNormal.y;
			contact2World.data[1] = axis2.x;
			contact2World.data[4] = axis2.y;
			contact2World.data[8] = 1.0f;
		}
		
		/* ----------------------------------------------------------
		 * Calculates and returns the velocity of the contact point
		 * of the given body
		 * ----------------------------------------------------------
		 */
		public Vector2 calculateLocalVelocity(int bodyIndex, float dt) {
			
			RigidBody body;
			Vector2 relativeContactPos;
			Vector2 velocity;
			
			// Retrieve which body to use
			if(bodyIndex == 1)	{body=rb1;relativeContactPos=relContactPos1;}
			else				{body=rb2;relativeContactPos=relContactPos2;}
			
			// Work out velocity of the contact point
			velocity = new Vector2(-relativeContactPos.y,relativeContactPos.x);
			velocity.mult(body.getRotation());
			velocity.add(body.getVelocity());
			
			// Turn the velocity into contact coordinates
			Vector2 thisContactVelocity = contact2World.invTransDir(velocity);
			
			// Calculate the amount of velocity that is due to forces without
			// reactions
			Vector2 accVelocity = body.getLastFrameAcc().multNew(dt);
			thisContactVelocity.sub(accVelocity);
			
			// And return it
			return thisContactVelocity;
		}
		
		/* ----------------------------------------------------------
		 * Calculate the desired change in velocity for resolution
		 * ----------------------------------------------------------
		 */
		public void calculateDesiredDeltaVelocity(float dt) {
			float velocityLimit = 0.25f;
			
			// Calculate the acceleration-induced velocity accumulated in this frame
			Vector2 scaledContact = contactNormal.multNew(dt);
			
			float	velocityFromAcc = rb1.getLastFrameAcc().dot(scaledContact);
			if(rb2!=null) {
					velocityFromAcc -= rb2.getLastFrameAcc().dot(scaledContact);
			}
			
			// If the velocity is very slow, limit the restitution
			float thisRestitution = restitution;
			if(Math.abs(contactVelocity.x) < velocityLimit) {
				thisRestitution = 0.0f;
			}
			
			// Combine the bounce velocity with removed acceleration velocity
			desiredDeltaVel =	-contactVelocity.x - thisRestitution*
								(contactVelocity.x - velocityFromAcc);
			
		}
		
		/* ----------------------------------------------------------
		 * 
		 * ----------------------------------------------------------
		 */
		public void calculateDeltaVelocity() {
			
			// Build a vector that shows the change in velocity in world space for a
			// unit impulse in the direction of the contact normal
			float angularComponent	= relContactPos1.cross(contactNormal);
			angularComponent		= rb1.inverseInertia*angularComponent;
			Vector2 deltaVelWorld	= new Vector2(
				-relContactPos1.y*angularComponent,
				relContactPos1.x*angularComponent);
			
			// Work out the change of velocity in contact coordinates(=along normal)
			deltaVelocity = deltaVelWorld.dot(contactNormal);
			// Add the linear component of velocity change (inverse mass)
			deltaVelocity += rb1.getInverseMass();

			// Check if other body exists and is non stationary
			if( rb2 != null && !rb2.isStatic()) {
				angularComponent= relContactPos2.cross(contactNormal);
				angularComponent= rb2.inverseInertia*angularComponent;
				deltaVelWorld	= new Vector2( -relContactPos2.y*angularComponent,
												relContactPos2.x*angularComponent);

				// Work out the change of velocity in contact coordinates(=along normal)
				deltaVelocity += deltaVelWorld.dot(contactNormal);
				// Add the liniear component of velocity change (inverse mass)
				deltaVelocity += rb2.getInverseMass();
			}
		}
		
		
	} // End of inner class Contact
	
	/* -------------------------------------------------------------------------
	 * Inner abstract class ContactGeometry
	 * -------------------------------------------------------------------------
	 */
	public static abstract class ContactGeometry {
		RigidBody body;	// Body this geometry belongs to
		Matrix3 offset; // Offset from body center
		
		/* ----------------------------------------------------------
		* Some abstract functions
		* -----------------------------------------------------------
		*/
		public abstract boolean	collision(	ContactGeometry other,
											ArrayList contacts);
		public abstract NarrowCollision.Type	getType();
		public abstract void render(Graphics2D _g2d);
		
		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public Matrix3 getTransform() {
			return body.transformMatrix.cross(offset);
		}
		
		/* ----------------------------------------------------------
		 * Collide a Circle with a rectangle
		 * ----------------------------------------------------------
		 */
		public boolean collision(	ContactCircle circle,
									ContactRectangle rectangle,
									ArrayList contacts) {
			
			// Get some useful stuff
			RigidBody rbc	= circle.body;
			RigidBody rbr	= rectangle.body;
			Matrix3 coffset = circle.offset;
			Matrix3 roffset = rectangle.offset;
			
			// First transform circle center to rectangle coordinates
			Vector2 ccenter = new Vector2();				// Center at [0,0]
			coffset.transUpdate(ccenter);					// In circle coord
			rbc.transformMatrix.transUpdate(ccenter);		// World coord
			ccenter=rbr.transformMatrix.invTrans(ccenter);	// Other body coor
			ccenter=roffset.invTrans(ccenter);				// Rect coord
			
			// Check if we are not in contact
			if( Math.abs(ccenter.x) - circle.radius > rectangle.halfSize.x ||
				Math.abs(ccenter.y) - circle.radius > rectangle.halfSize.y) {
				return false;
			}
			
			// Maybe we are in contact, work out collision data
			Vector2 closestPoint = new Vector2();
			float dist;
			
			// Clamp each coordinate to rectangle
			dist = ccenter.x;
			if(dist > rectangle.halfSize.x)		{dist = rectangle.halfSize.x;}
			if(dist < -rectangle.halfSize.x)	{dist = -rectangle.halfSize.x;}
			closestPoint.x = dist;
			
			dist = ccenter.y;
			if(dist > rectangle.halfSize.y)		{dist = rectangle.halfSize.y;}
			if(dist < -rectangle.halfSize.y)	{dist = -rectangle.halfSize.y;}
			closestPoint.y = dist;
			
			// Well? Are we in contact?
			dist = closestPoint.subNew(ccenter).sqrdMagnitude();
			if(dist > circle.radius*circle.radius) {return false;}
			
			// Contact! Create the contact data!
			Contact contact = new Contact();
			roffset.transUpdate(ccenter);
			rbr.transformMatrix.transUpdate(ccenter);
			//System.out.println("Ccenter: " + ccenter + "\n");
			//System.out.println("Rcenter: " + rbr.pos + "\n");
			
			// Contact point in world coord
			Vector2 closestPtWorld = roffset.trans(closestPoint);
			rbr.transformMatrix.transUpdate(closestPtWorld);
			contact.contactPoint	= closestPtWorld;
			contact.contactNormal	= closestPtWorld.subNew(ccenter);
			contact.contactNormal.normalize();
			contact.penetration		= circle.radius - (float)Math.sqrt(dist);
			
			//System.out.println("radius: " + circle.radius + "\n");
			//System.out.println("Dist:   " + (float)Math.sqrt(dist) + "\n");
			
			if(contact.contactNormal.x != contact.contactNormal.x) {
				contact.contactNormal.x = 0.0f;
				contact.contactNormal.y = 1.0f;
			}
			
			contact.rb1 = rbc;
			contact.rb2 = rbr;
			contacts.add(contact);
			return true;
		}
		
	}
	
	/* -------------------------------------------------------------------------
	 * ContactCircle
	 * -------------------------------------------------------------------------
	 */
	public static class ContactCircle extends ContactGeometry {
		float radius;
		
		/* ----------------------------------------------------------
		 * Constructor
		 * ----------------------------------------------------------
		 */
		public ContactCircle() {
			body = null; offset = new Matrix3(); offset.eye(); radius = 10.0f;
		}
		
		public ContactCircle(RigidBody rb, float r) {
			body = rb; offset = new Matrix3(); offset.eye(); radius = r;
		}
		
		public ContactCircle(RigidBody rb, Matrix3 o, float r) {
			body = rb; offset = new Matrix3(o); radius = r;
		}
		
		public ContactCircle(ContactCircle cc) {
			body = new RigidBody(cc.body);
			offset = new Matrix3(cc.offset);
			radius = cc.radius;
		}
		
		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public NarrowCollision.Type getType() {return Type.CIRCLE;}
		
		/* ----------------------------------------------------------
		 * Collision
		 * ----------------------------------------------------------
		 */
		public boolean collision(ContactGeometry other, ArrayList contacts) {
			
			/* Circle with circle collision */
			if( other.getType() == Type.CIRCLE ) {
				// Cast other object to circle object
				ContactCircle otherCircle = (ContactCircle)other;
				return collision(otherCircle,contacts);
				
			/* Circle with rectangle collision */
			} else if(  other.getType() == Type.RECTANGLE ) {
				// Cast other object to rectangle object
				ContactRectangle rectangle = (ContactRectangle)other;
				return collision(this, rectangle, contacts);
			}
			
			// Something unexpected has happened
			return false;
		
		}
		
		/* ----------------------------------------------------------
		 * Collide this circle with another circle
		 * ----------------------------------------------------------
		 */
		public boolean collision(ContactCircle other, ArrayList contacts) {
			
			// Get some objects needed
			RigidBody one = this.body;	// This body
			RigidBody two = other.body;	// Other body
			
			// Convert this circle to world coordinates
			Vector2 thisCenter = new Vector2();					// Geom coord
			offset.transUpdate(thisCenter);						// Body coord
			thisCenter = one.getPointInWorldSpace(thisCenter); // World coord
			
			// Convert other circle to world coordinates
			Vector2 otherCenter = new Vector2();				// Geom coord
			other.offset.transUpdate(otherCenter);				// Body coord
			otherCenter = two.getPointInWorldSpace(otherCenter); // World coord
			
			//System.out.println("Center1: "+thisCenter+" Center2: "+otherCenter);
			
			// Calculate distance to see if collision occured or not
			Vector2 midline = new Vector2(otherCenter);
			midline.sub(thisCenter);
			float size = midline.magnitude();
			if(size<=0.0f || size >= this.radius+other.radius ) {return false;}
			
			// Generate contact data
			Contact contact = new Contact();
			contact.contactNormal	= midline.multNew(1.0f/size); //contact.contactNormal.invert();
			midline.mult(0.5f);
			contact.contactPoint	= thisCenter.addNew(midline);
			contact.penetration		= (this.radius+other.radius) - size;
			contact.rb1 = one;
			contact.rb2 = two;
			contact.type = 1;
			
			contacts.add(contact);
			return true;
		}
		
		/* ----------------------------------------------------------
		 * Render
		 * ----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d) {
			
			// Get a transform to keep and one to change
			AffineTransform stack = _g2d.getTransform();
			AffineTransform trans = _g2d.getTransform();
			
			// Render circle at the circle center
			Vector2 center = new Vector2();
			offset.transUpdate(center);
			body.transformMatrix.transUpdate(center);
			int x = (int)(center.x-radius);
			int y = (int)(center.y-radius);
			_g2d.setColor(Color.RED);
			trans.rotate(body.getOrientationRad(), center.x, center.y);
			_g2d.setTransform(trans);
			_g2d.drawOval(x,y,(int)radius*2, (int)radius*2);
			
			// Reset transform
			_g2d.setTransform(stack);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * ContactRectangle
	 * -------------------------------------------------------------------------
	 */
	public static class ContactRectangle extends ContactGeometry {
		Vector2 halfSize;
		
		/* ----------------------------------------------------------
		 * Constructors
		 * ----------------------------------------------------------
		 */
		public ContactRectangle() {
			body		= null;
			offset		= new Matrix3(); offset.eye();
			halfSize	= new Vector2(10.0f, 10.0f);
		}
		
		public ContactRectangle(RigidBody rb, float x, float y) {
			body		= rb; offset = new Matrix3(); offset.eye();
			halfSize	= new Vector2(x, y);
		}
		
		public ContactRectangle(RigidBody rb, Matrix3 o, float x, float y) {
			body		= rb;
			offset		= new Matrix3(o);
			halfSize	= new Vector2(x,y);
		}
		
		public ContactRectangle(ContactRectangle cr) {
			body	 = new RigidBody(cr.body);
			offset	 = new Matrix3(cr.offset);
			halfSize = new Vector2(cr.halfSize);
		}
		
		/* ----------------------------------------------------------
		 * Getters and setters
		 * ----------------------------------------------------------
		 */
		public Type getType() {return Type.RECTANGLE;}
		
		public Vector2 getAxis(int index) {
			return getTransform().getAxisVector(index);
		}
		
		/* ----------------------------------------------------------
		 * Collision
		 * ----------------------------------------------------------
		 */
		public boolean collision(ContactGeometry other, ArrayList contacts) {
			
			/* Circle with circle collision */
			if( other.getType() == Type.RECTANGLE ) {
				// Cast other object to circle object
				ContactRectangle otherRect = (ContactRectangle)other;
				return collision(otherRect,contacts);
				
			/* Circle with rectangle collision */
			} else if(  other.getType() == Type.CIRCLE ) {
				// Cast other object to circle object
				ContactCircle circle = (ContactCircle)other;
				return collision(circle, this, contacts);
			}
			
			// Something unexpected has happened
			return false;
		
		}
		
		/* ----------------------------------------------------------
		 * Collide this rectangle with another rectangle using a 
		 * separate axis test.
		 * Axis with smallest overlap provides contact point, normal
		 * and penetration.
		 * 
		 * NOTE:Ok...seems to work ok for vertex-edge cases
		 *		Performs soso when boxes has parallell edges it seems
		 * 
		 * ----------------------------------------------------------
		 */
		public boolean collision(ContactRectangle other, ArrayList contacts) {
			
			// Used to store contact data
			float overlap, bestOverlap	= 10000.0f;	// Large number....
			Vector2 contactPoint		= new Vector2();
			Vector2 contactNormal		= new Vector2();
			int bestCase				= -1;
			
			// Get a vector from center one to center two
			Vector2 center1 = this.getTransform().trans(new Vector2());
			Vector2 center2 = other.getTransform().trans(new Vector2());
			Vector2 toCenter = center2.subNew(center1);
			
			// Create matrices for transforms from rectangle space 2 world space
			Matrix3 this2World	= this.getTransform();
			Matrix3 other2World = other.getTransform();
			
			// Create the four vectors that make up the test axises.
			Vector2 axisA1 = this.getAxis(0);	axisA1.normalize();
			Vector2 axisA2 = this.getAxis(1);	axisA2.normalize();
			Vector2 axisB1 = other.getAxis(0);	axisB1.normalize();
			Vector2 axisB2 = other.getAxis(1);	axisB2.normalize();
			Vector2[] axises = {axisA1, axisA2, axisB1, axisB2};
			
			// Test against axises...
			for(int i=0;i<4;i++) {
				overlap = penetrationOnAxis(this, other, axises[i], toCenter);
				if( overlap < 0.0f )	{return false;}
				else{
					if(overlap<bestOverlap) {
						bestOverlap = overlap;
						bestCase = i;
					}
				}
			} // End of for
			
			int index = bestCase;
			ContactRectangle one, two;
			if(bestCase < 2)	{one = this; two = other;}
			else {one = other; two = this; toCenter.invert(); index-=2;}
			
			// Get normal
			Vector2 normal = one.getAxis(index); normal.normalize(); //System.out.println(normal);
			
			// Which edge is in contact
			if( normal.dot(toCenter) < 0.0f ) {normal.invert();}
				
			// Which vertex is in contact (in others coord)
			Vector2 vertex = new Vector2(two.halfSize);
			if( two.getAxis(0).dot(normal) > 0.0f){vertex.x=-vertex.x;}
			if( two.getAxis(1).dot(normal) > 0.0f){vertex.y=-vertex.y;}
			two.getTransform().transUpdate(vertex);
			
			// Build contact data
			Contact contact = new Contact();
			contact.contactNormal = normal;
			contact.contactPoint  = vertex;
			contact.penetration = bestOverlap;
			contact.rb1 = one.body;
			contact.rb2 = two.body;
			contacts.add(contact);
			
			return true;
		}
		
		/* ----------------------------------------------------------
		 * Help functions when rectangle-rectangle collision
		 * ----------------------------------------------------------
		 */
		private float penetrationOnAxis(ContactRectangle one,
										ContactRectangle two,
										Vector2 axis, Vector2 toCenter) {
			
			// Project the half-size of one onto axis
			float oneProject = transformToAxis(one, axis);
			float twoProject = transformToAxis(two, axis);

			// Project this onto the axis
			float distance = Math.abs(toCenter.dot(axis));

			// Check for overlap
			return oneProject + twoProject - distance;
		}
		
		/* */
		public float transformToAxis(ContactRectangle rec, Vector2 axis) {
			return
				rec.halfSize.x * Math.abs(axis.dot(rec.getAxis(0))) +
				rec.halfSize.y * Math.abs(axis.dot(rec.getAxis(1)));
		}
		
		/* ----------------------------------------------------------
		 * Collide two rectangles with parallell sides
		 * 
		 * ----------------------------------------------------------
		 */
		
		/* ----------------------------------------------------------
		 * Render
		 * ----------------------------------------------------------
		 */
		public void render(Graphics2D _g2d) {
			Matrix3 thisTrans	= body.transformMatrix;
			float theta			= body.getOrientationRad();
						
			AffineTransform stack	= _g2d.getTransform();
			AffineTransform trans	= _g2d.getTransform();
			Vector2 center = new Vector2();
			
			_g2d.setColor(Color.RED);
			
			Vector2 pos = thisTrans.trans(center);
			
			// For drawing rectangles at correct position
			int drawX	= (int)(pos.x - halfSize.x);
			int drawY	= (int)(pos.y - halfSize.y);
			
			// For rotating
			int centerX	= (int)(pos.x);
			int centerY	= (int)(pos.y);
			
			// transform before rendering
			trans.rotate(theta, centerX, centerY); // Rotate around center
			_g2d.setTransform(trans); // Apply transform
			_g2d.drawRect(drawX, drawY, (int)halfSize.x*2, (int)halfSize.y*2);
			
			// Restore the transform
			_g2d.setTransform(stack);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Builds a list of actual contacts that can be sent to the resolution
	 * system.
	 * -------------------------------------------------------------------------
	 */
	public void buildContactList(ArrayList potentialContacts) {
		
		CoarseCollision.PotentialContact current;
		RigidBody rb1, rb2;
		int length = potentialContacts.size();
		
		for(int i=0;i<length;i++) {
			current = (CoarseCollision.PotentialContact)potentialContacts.get(i);
			rb1 = current.rb1;
			rb2 = current.rb2;
			
			if( rb1.contactGeom.collision(rb2.contactGeom, contacts) ) {
				currentNrOfCollisions++;
			}
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Calculates all internal data for all contacts in the current list of
	 * found contacts
	 * -------------------------------------------------------------------------
	 */
	public void calculateAllInternals(float dt) {
		int length = contacts.size();
		Contact currentContact;
		
		for(int i=0;i<length;i++) {
			currentContact = (Contact)contacts.get(i);
			currentContact.calculateInternals(dt);
		}
	}
	
	/* -------------------------------------------------------------------------
	 * Getters and setters (typ...)
	 * -------------------------------------------------------------------------
	 */
	public void addContact(Contact contact) {contacts.add(contact);}
	public void clear() {contacts.clear();currentNrOfCollisions = 0;}
	
	/* -------------------------------------------------------------------------
	 * Render various contact information on screen
	 * -------------------------------------------------------------------------
	 */
	public void render(Graphics2D _g2d) {
		
		int length = contacts.size();
		Contact contact;
		
		for(int i=0; i<length;i++) {
			contact = (Contact)contacts.get(i);
			if(contact.rb1!=null) {contact.rb1.contactGeom.render(_g2d);}
			if(contact.rb2!=null) {contact.rb2.contactGeom.render(_g2d);}
			
			_g2d.setColor(Color.WHITE);
			Vector2 start	= new Vector2(contact.contactPoint.x, contact.contactPoint.y);
			Vector2 end		= new Vector2(start.addNew(contact.contactNormal.multNew(20)));
			//_g2d.fillOval((int)start.x, (int)start.y, 3,3);
			_g2d.drawLine((int)start.x, (int)start.y, (int)end.x, (int)end.y);
			_g2d.drawLine((int)start.x+2, (int)start.y+2, (int)end.x, (int)end.y);
			_g2d.drawLine((int)start.x-2, (int)start.y-2, (int)end.x, (int)end.y);
		}
	}
	
}
