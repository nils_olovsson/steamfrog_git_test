========================
Steam Frog v.0.1
24-08-2012
========================
In this folder there should exist four things:
This file, one folder named "levels", 
one folder
named "materials" and a file "SteamFrog.jar".
To launch game, double click on SteamFrog.jar.

An online level editor is temporarily hosted at:
http://www.dynamicvertigo.se/LevelEditor/
To load your level into the game, save it to the "levels"-folder,
start the game and skip levels until it shows up.

Walk					- Left/Right arrow keys.
Jump					- Space or key up
Pick up crates and cogs	- Ctrl
Pause/Unpause			- P
Reload level			- R
Jump to next level		- N
Exit					- Escape

Game provided as is, known bugs are:
-Unstable physics during multi object contacts.
 Resulting in objects that starts to vibrate heavily and
 potentially can pass through other objects.
-Semi poor memory management. Can lead to out of memory on heap exception. Game crashes.
-Game looses connection to input. Results in no response from game despite user input.

Contributors:
Daniel Svedlund		- Sound programmer
Diana Ekström Ho	- Level parser programmer, Level layout management
Hanna Björgvinsdottir	- Gameplay programmer, Sprite-loading programmer
Mattias Andersson	- Level editor programmer, Artwork, Music, Level layout management
Nils Olofsson		- Gameplay programmer, Physics programmer
